import './css/app.scss';

import $ from 'jquery';
import 'bootstrap';
import 'ckeditor';
import 'slick-carousel/slick/slick';

window.jQuery = $;

require("@fancyapps/fancybox");

var $collectionHolder;

// setup an "add a tag" link
var $addTagButton = $('<button type="button" class="add_tag_link btn  btn-danger ">Agregar</button></div>');
var $newLinkLi = $('<div class="m-t-2"></div>').append($addTagButton);

$(document).ready(function () {
    let sliders = {};

    if ($(".js-slick").length) {
        $(".js-slick").each(function (index, value) {
            var sliderName = $(this).data('slider-name') || 'slider-' + index;
            sliders[sliderName] = $(this).slick();
        });
    }
    if ($(".alert-nfancy").length) {
        $.fancybox.open('<div class="message text-center"><h3>Solicitud creada</h3><button type="button" class="btn btn-danger m-t-2 btn-block py-4 m-t-4" data-fancybox-close="">CONTINUAR</button></div>');
    }
    if ($(".alert-alreadyrenewed").length) {
        $.fancybox.open('<div class="message text-center"><h3 class="txt-red">La solicitud no pudo ser renovada</h3><p>Ya cuenta con una renovación en el ciclo actual</p><button type="button" class="btn btn-danger m-t-2 btn-block py-4 m-t-4" data-fancybox-close="">CONTINUAR</button></div>');
    }


    if ($(".fancybox").length) {
        $(".fancybox").fancybox({
        });
    }
    $('.f-close').click(function () {
        console.log("hols")
        $.fancybox.close();
    });

    $('.loader').click(function () {
        $(".btn-out").prop("disabled", true);
        if (requisition.reportValidity()) {
            $(".form-horizontal").submit();
        }
        setTimeout(
                function ()
                {
                    $(".btn-out").prop("disabled", false);

                }, 3500);
        return true;
    });







//    $('.choice-buttons input:checked').prev().addClass('active');
    $('.choice-buttons input').change(function (event) {
        //value = $(this).val();
        $('.choice-buttons label').removeClass('active');
        $(this).prev().addClass('active');
    });

    addLinks();
    var $requistionField = $('#requisition_isCoupon');
    $requistionField.change(function () {
        var $form = $(this).closest('form');
        var data = {};
        data[$requistionField.attr('name')] = $requistionField.val();
        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            data: data,
            success: function (html) {
                $('#js-collection').replaceWith(
                        $(html).find('#js-collection')
                        );
                addLinks();
            }
        });
    });
    $(document).on('change', 'select.select-changer', function () {
        var p = $(this).closest('.row');
        var qty = p.find('.inpt-qty');
        var option = $(this).find('option:selected');
        var max = option.data('max-qty');
        $(qty).find('option').not(':first').remove();
        for (var i = 1; i <= max; i++) {
            $(qty).append($('<option>',
                    {
                        value: i,
                        text: i
                    }));
        }
    });
    var selects = $('form.filter-form').find('select');
    if (selects.length > 0) {
        $(document).on('change', selects, function () {
//        $('.is-coupon').click(function () {
            $('.filter-body').fadeTo('fast', '.2');
            var form = $('form.filter-form');
            $.ajax({
                data: form.serialize(),
                method: 'POST',
                dataType: 'html',
                success: function (html) {
                    $('.filter-body').html(html);
                },
                error: function (xhr, status) {
                    alert('Disculpe, existió un problema');
                },
                complete: function (xhr, status) {
                    $('.filter-body').fadeTo('fast', 1);
                }
            });
        });
    }
    $('.cupon').click(function () {
        $('#requisition_isCoupon').val(1).change();
        $('.normal').removeClass('active');
        $('.cupon').addClass('active');
    });
    $('.normal').click(function () {

        $('#requisition_isCoupon').val(0).change();
        $('.cupon').removeClass('active');
        $('.normal').addClass('active');
    });

    $('button.modal-btn').click(function () {
        var action = $(this).data('action');
        $('.modal-action').attr('href', action);
    });

});



function addTagForm($collectionHolder, $newLinkLi) {
    if ($(".alert-warning").length) {
        $(".alert-warning").remove();
    }
    var prototype = $collectionHolder.data('prototype');
    var index = $collectionHolder.data('index');
    var newForm = prototype;
    newForm = newForm.replace(/__name__/g, index);
    $collectionHolder.data('index', index + 1);
    var $newFormLi = $('<div class="m-t-3"></div>').append(newForm);
    $newLinkLi.before($newFormLi);
    $('.remove-tag').click(function (e) {
        e.preventDefault();
        $(this).parent().remove();
        return false;
    });

}

function addLinks() {
    $collectionHolder = $('.js-collection-holder');
    $collectionHolder.append($newLinkLi);
    $collectionHolder.data('index', $collectionHolder.find('.row').length);
    $addTagButton.on('click', function (e) {
        addTagForm($collectionHolder, $newLinkLi);
    });
}

