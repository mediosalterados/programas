<?php

namespace App\Form;

use App\Entity\Cycle;
use App\Entity\Direccion;
use App\Entity\Doctor;
use App\Repository\RequisitionRepository;
use App\Services\CycleManager;
use App\Services\DataHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Security;

class ManagerFilterType extends AbstractType {
    private $requisitionRepository;
    private $user;
    private $cycleManager;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DataHelper
     */
    private $dataHelper;

    public function __construct(RequisitionRepository $requisitionRepository, DataHelper $dataHelper,Security $security, CycleManager $cycleManager, EntityManagerInterface $entityManager) {
        $this->requisitionRepository = $requisitionRepository;
        $this->user = $security->getUser();
        $this->cycleManager = $cycleManager;
        $this->entityManager = $entityManager;
        $this->dataHelper = $dataHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $ciclosQuery = $this->entityManager->getRepository(Cycle::class)->findCiclosByManager($this->user);
        $queryDoctors = $this->entityManager->getRepository(Doctor::class)->findDoctorsByManager($this->user,$this->dataHelper->getParameters());
        $builder
            ->add('medico', EntityType::class, ['query_builder' => $queryDoctors, 'class' => Doctor::class, 'required' => false, 'placeholder' => 'Seleccionar médico'])
            ->add('ciclos', EntityType::class, ['query_builder' => $ciclosQuery, 'required' => false, 'class' => Cycle::class, 'placeholder' => 'Seleccionar un ciclo']);
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();
            if (!$data['medico']) {
                $queryDoctors = $this->entityManager->getRepository(Doctor::class)->findDoctorsByUser($this->user, $data);
                $form->add('medico', EntityType::class, ['query_builder' => $queryDoctors, 'class' => Doctor::class, 'required' => false, 'placeholder' => 'Seleccionar médico']);
            }
            if (!$data['ciclos']) {
                $ciclosQuery = $this->entityManager->getRepository(Cycle::class)->findCiclosByUser($this->user, $data);
                $form->add('ciclos', EntityType::class, ['query_builder' => $ciclosQuery, 'required' => false, 'class' => Cycle::class, 'placeholder' => 'Seleccionar un ciclo']);
            }
        });
    }
}
