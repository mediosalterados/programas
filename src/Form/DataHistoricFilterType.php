<?php

namespace App\Form;

use App\Entity\Cycle;
use App\Entity\Direccion;
use App\Entity\Doctor;
use App\Entity\Program;
use App\Repository\RequisitionRepository;
use App\Services\CycleManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class DataHistoricFilterType extends AbstractType {

    private $requisitionRepository;
    private $user;
    private $cycleManager;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(RequisitionRepository $requisitionRepository, Security $security, CycleManager $cycleManager, EntityManagerInterface $entityManager) {
        $this->requisitionRepository = $requisitionRepository;
        $this->user = $security->getUser();
        $this->cycleManager = $cycleManager;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $choicesBenefectars = $this->requisitionRepository->findBenefectarsByUser($this->user);
        $choicesBenefectarsValue = array_column($choicesBenefectars, 'beneficiary');
        $statusLabel = array('renewed' => 'Renovado', 'to_renovate' => 'Por renovar', 'suspended' => 'Suspendida', 'canceled' => 'Cancelada');
        $choicesStatus = $this->requisitionRepository->findStatusByUser($this->user);
        $choicesStatusValue = array_column($choicesStatus, 'status');
        
        foreach ($choicesStatusValue as $valor) {
            $choicesStatusLabel[] = $statusLabel[$valor];
        }
        
        if ( $choicesStatusValue == NUll) {
            $arreglo = array();
        } else {
            $arreglo = array_combine($choicesStatusLabel, $choicesStatusValue);
        }

        $ciclosQuery = $this->entityManager->getRepository(Cycle::class)->findCiclosByUser($this->user);
        $queryPrograms = $this->entityManager->getRepository(Program::class)->findProgramsByUser($this->user);
        $queryDoctors = $this->entityManager->getRepository(Doctor::class)->findDoctorsByUser($this->user);
        $builder
                ->add('medico', EntityType::class, ['query_builder' => $queryDoctors, 'class' => Doctor::class, 'required' => false, 'placeholder' => 'Seleccionar médico'])
                ->add('programa', EntityType::class, ['query_builder' => $queryPrograms, 'class' => Program::class, 'required' => false, 'placeholder' => 'Seleccionar un programa'])
                ->add('beneficiario', ChoiceType::class, ['required' => false, 'choices' => array_combine($choicesBenefectarsValue, $choicesBenefectarsValue), 'placeholder' => 'Seleccionar un beneficiario'])
                ->add('ciclos', EntityType::class, ['query_builder' => $ciclosQuery, 'required' => false, 'class' => Cycle::class, 'placeholder' => 'Seleccionar un ciclo'])
                ->add('status', ChoiceType::class, ['required' => false, 'choices' => $arreglo, 'placeholder' => 'Seleccionar un status']);
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();
            if (!$data['medico']) {
                $queryDoctors = $this->entityManager->getRepository(Doctor::class)->findDoctorsByUser($this->user, $data);
                $form->add('medico', EntityType::class, ['query_builder' => $queryDoctors, 'class' => Doctor::class, 'required' => false, 'placeholder' => 'Seleccionar médico']);
            }

            if (!$data['programa']) {
                $queryPrograms = $this->entityManager->getRepository(Program::class)->findProgramsByUser($this->user, $data);
                $form->add('programa', EntityType::class, ['query_builder' => $queryPrograms, 'class' => Program::class, 'required' => false, 'placeholder' => 'Seleccionar un programa']);
            }

            if (!$data['beneficiario']) {
                $choicesBenefectars = $this->requisitionRepository->findBenefectarsByUser($this->user, $data);
                $choicesBenefectarsvalue = array_column($choicesBenefectars, 'beneficiary');
                $form->add('beneficiario', ChoiceType::class, ['required' => false, 'choices' => array_combine($choicesBenefectarsvalue, $choicesBenefectarsvalue), 'placeholder' => 'Seleccionar un beneficiario']);
            }

            if (!$data['ciclos']) {
                $ciclosQuery = $this->entityManager->getRepository(Cycle::class)->findCiclosByUser($this->user, $data);
                $form->add('ciclos', EntityType::class, ['query_builder' => $ciclosQuery, 'required' => false, 'class' => Cycle::class, 'placeholder' => 'Seleccionar un ciclo']);
            }

            if (!$data['status']) {
                $choicesStatus = $this->requisitionRepository->findStatusByUser($this->user, $data);
                $choicesStatusValue = array_column($choicesStatus, 'status');
                $form->add('status', ChoiceType::class, ['required' => false, 'choices' => array_combine($choicesStatusValue, $choicesStatusValue), 'placeholder' => 'Seleccionar un status']);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
                // Configure your form options here
        ]);
    }

}
