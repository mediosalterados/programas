<?php

namespace App\Form;

use App\Entity\Direccion;
use App\Entity\Doctor;
use App\Entity\Program;
use App\Repository\RequisitionRepository;
use App\Services\CycleManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class DataFilterType extends AbstractType
{
    private $requisitionRepository;
    private $user;
    private $cycleManager;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(RequisitionRepository $requisitionRepository, Security $security, CycleManager $cycleManager, EntityManagerInterface $entityManager)
    {
        $this->requisitionRepository = $requisitionRepository;
        $this->user = $security->getUser();
        $this->cycleManager = $cycleManager;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $parameters = null;
        if($this->cycleManager->isUseCurrentCycle()){
            $parameters['isActive'] = true;
            $parameters['ciclos'] = $this->cycleManager->getCurrentCycle();
        }

        $choicesBenefectars = $this->requisitionRepository->findBenefectarsByUser($this->user,$parameters);
        $choicesBenefectarsvalue = array_column($choicesBenefectars, 'beneficiary');


        $queryPrograms = $this->entityManager->getRepository(Program::class)->findProgramsByUser($this->user);
        $queryDoctors = $this->entityManager->getRepository(Doctor::class)->findDoctorsByUser($this->user);

        $builder
            ->add('medico', EntityType::class, ['query_builder' => $queryDoctors, 'class' => Doctor::class, 'required' => false, 'placeholder' => 'Seleccionar médico'])
            ->add('programa', EntityType::class, ['query_builder' => $queryPrograms, 'class' => Program::class, 'required' => false, 'placeholder' => 'Seleccionar un programa'])
            ->add('beneficiario', ChoiceType::class, ['required' => false, 'choices' => array_combine($choicesBenefectarsvalue, $choicesBenefectarsvalue), 'placeholder' => 'Seleccionar un beneficiario']);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();

            if (!$data['medico']) {
                $parameters = array('programa' => $data['programa'], 'beneficiario' => $data['beneficiario']);
                $queryDoctors = $this->entityManager->getRepository(Doctor::class)->findDoctorsByUser($this->user, $parameters);
                $form->add('medico', EntityType::class, ['query_builder' => $queryDoctors, 'class' => Doctor::class, 'required' => false, 'placeholder' => 'Seleccionar médico']);
            }

            if (!$data['programa']) {
                $parameters = array('medico' => $data['medico'], 'beneficiario' => $data['beneficiario']);
                $queryPrograms = $this->entityManager->getRepository(Program::class)->findProgramsByUser($this->user,$parameters);
                $form->add('programa', EntityType::class, ['query_builder' => $queryPrograms, 'class' => Program::class, 'required' => false, 'placeholder' => 'Seleccionar un programa']);
            }

            if (!$data['beneficiario']) {
                $parameters = array('medico' => $data['medico'], 'programa' => $data['programa']);
                $choicesBenefectars = $this->requisitionRepository->findBenefectarsByUser($this->user,$parameters);
                $choicesBenefectarsvalue = array_column($choicesBenefectars, 'beneficiary');
                $form->add('beneficiario', ChoiceType::class, ['required' => false, 'choices' => array_combine($choicesBenefectarsvalue, $choicesBenefectarsvalue), 'placeholder' => 'Seleccionar un beneficiario']);
            }

        });


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
