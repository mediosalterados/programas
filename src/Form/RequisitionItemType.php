<?php

namespace App\Form;

use App\Entity\Direccion;
use App\Entity\Medicine;
use App\Entity\RequisitionItem;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class RequisitionItemType extends AbstractType
{

    private $user;

    public function __construct(Security $security)
    {
        $this->user = $security->getUser();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = [];
        $label = '¿Cuantas al dia?';
        $builder
            ->add('medicine', EntityType::class, ['placeholder' => 'Selecciona medicamento', 'expanded' => false, 'choice_attr' => function ($choice, $key, $value) {
                return ['data-max-qty' => $choice->getMaximumPerDose()];
            },
                'class' => Medicine::class, 'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->where('m.lineas LIKE :linea')
                        ->setParameter('linea', '%"' . $this->user->getLinea() . '"%');
                }])
            ->add('quantity', ChoiceType::class, ['placeholder' => $label, 'required' => true, 'choices' => array_combine($choices, $choices)])
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($label) {
                /** @var RequisitionItem $data */
                $data = $event->getData();
                $form = $event->getForm();
                if (!$data) {
                    return;
                }
                $choices = range(1, $data->getMedicine()->getMaximumPerDose());
                $form->add('quantity', ChoiceType::class, ['placeholder' => $label, 'required' => true, 'choices' => array_combine($choices, $choices), 'data' => $data->getQuantity()]);
            });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($label) {
            $data = $event->getData();
            $form = $event->getForm();
            $choices = [1];
            if (isset($data['quantity'])) {
                $choices = [$data['quantity']];
            }
            $form->add('quantity', ChoiceType::class, ['placeholder' => $label, 'required' => false, 'choices' => array_combine($choices, $choices)]);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RequisitionItem::class,
        ]);
    }

}
