<?php

namespace App\Form;

use App\Entity\Cycle;
use App\Entity\Direccion;
use App\Entity\Doctor;
use App\Entity\Program;
use App\Repository\CycleRepository;
use App\Repository\RequisitionRepository;
use App\Services\CycleManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class MedicineFilterType extends AbstractType
{
    /**
     * @var CycleRepository
     */
    private $cycleRepository;

    public function __construct(CycleRepository $cycleRepository)
    {

        $this->cycleRepository = $cycleRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $parameters = null;
        $builder
            ->add('ciclo', EntityType::class, ['class' => Cycle::class, 'required' => false, 'placeholder' => 'Seleccionar ciclo']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
