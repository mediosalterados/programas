<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Id\UuidGenerator;
use Ramsey\Uuid\Uuid;

class UserPasswordType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
//                ->add('currentpassword', 'password', array('label' => 'Current password', 'mapped' => false, 'constraints' => new UserPassword()))
                ->add('oldPassword', PasswordType::class, array(
                    'mapped' => false,
                    'required' => true,
                ))
                ->add('plainPassword', RepeatedType::class, array(
                    'type' => PasswordType::class,
                    'required' => false,
                    'first_options' => array('label' => 'Contraseña nuevo'),
                    'second_options' => array('label' => 'Confirma contraseña'),
                    'invalid_message' => 'Los campos de contraseña deben coincidir.',
                    'mapped' => false,
                    'required' => true,
        ));
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

}
