<?php

namespace App\Form;

use App\Entity\Doctor;
use App\Entity\Program;
use App\Entity\Requisition;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class RequisitionType extends AbstractType
{

    private $user;

    public function __construct(Security $security)
    {
        $this->user = $security->getUser();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('beneficiary', HiddenType::class, [
                'required' => false
            ])
            ->add('first', TextType::class, [
                'mapped' => false,
                'required' => true
            ])
            ->add('second', TextType::class, [
                'mapped' => false,
                'required' => true
            ])
            ->add('third', TextType::class, [
                'mapped' => false,
                'required' => true
            ])
            ->add('quarter', TextType::class, [
                'mapped' => false,
                'required' => false
            ])
            ->add('doctor', EntityType::class, [
                'class' => Doctor::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.ruta = :ruta')
                        ->orderBy('u.name', 'ASC')
                        ->setParameter('ruta', $this->user->getRuta());
                },
            ])
            ->add('program', EntityType::class, ['expanded' => true,
                'multiple' => false, 'class' => Program::class, 'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->where('p.lineas LIKE :linea')
                        ->orderBy('p.name', 'ASC')
                        ->setParameter('linea', '%"' . $this->user->getLinea() . '"%');
                }])
            ->add('isCoupon', ChoiceType::class, ['expanded' => false,
                'multiple' => false, 'choices' => ['Si' => 1, 'No' => 0]])
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                /** @var Requisition $data */
                $data = $event->getData();
                $form = $event->getForm();
                if (!$data) {
                    return;
                }
                if ($data->getIsCoupon()) {
                    $form->add('items', CollectionType::class, ['by_reference' => false, 'required' => true, 'entry_type' => RequisitionCouponItemType::class, 'entry_options' => ['label' => false], 'allow_delete' => true, 'allow_add' => true]);
                } else {
                    $form->add('items', CollectionType::class, ['by_reference' => false, 'required' => true, 'entry_type' => RequisitionItemType::class, 'entry_options' => ['label' => false], 'allow_delete' => true, 'allow_add' => true]);
                }
            })
            ->add('items', CollectionType::class, ['by_reference' => false, 'required' => true, 'entry_type' => RequisitionItemType::class, 'entry_options' => ['label' => false], 'allow_delete' => true, 'allow_add' => true]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Requisition::class,
        ]);
    }

}
