<?php

namespace App\Form;

use App\Entity\Requisition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class RequisitionEditType extends AbstractType
{

    private $user;

    public function __construct(Security $security)
    {
        $this->user = $security->getUser();
    }



    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
        /** @var Requisition $data */
        $data = $event->getData();
        $form = $event->getForm();
        if (!$data) {
            return;
        }

        if ($data->getIsCoupon()) {
            $form->add('items', CollectionType::class, ['by_reference' => false, 'required' => true, 'entry_type' => RequisitionCouponItemType::class, 'entry_options' => ['label' => false], 'allow_delete' => true, 'allow_add' => true]);
        } else {
            $form->add('items', CollectionType::class, ['by_reference' => false, 'required' => true, 'entry_type' => RequisitionItemType::class, 'entry_options' => ['label' => false], 'allow_delete' => true, 'allow_add' => true]);
        }
    })
        ->add('items', CollectionType::class, ['by_reference' => false, 'entry_type' => RequisitionItemType::class, 'entry_options' => ['label' => false], 'allow_delete' => true, 'allow_add' => true]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Requisition::class,
        ]);
    }

}
