<?php


namespace App\MessageHandler;


use App\Entity\Campaign;
use App\Entity\CampaignCode;
use App\Message\GeneratePdf;
use App\Message\ImportCode;
use App\Services\CampaignManager;
use App\Services\PdfGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GeneratePdfHandler implements MessageHandlerInterface
{


    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var PdfGenerator
     */
    private $pdfGenerator;

    public function __construct(EntityManagerInterface $em, PdfGenerator $pdfGenerator)
    {

        $this->em = $em;
        $this->pdfGenerator = $pdfGenerator;
    }

    public function __invoke(GeneratePdf $generatePdf)
    {
        $code = $this->em->getRepository(CampaignCode::class)->find($generatePdf->getCode());
        $this->pdfGenerator->generatePdf($code);
    }
}