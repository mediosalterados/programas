<?php


namespace App\MessageHandler;


use App\Entity\Campaign;
use App\Message\ImportCode;
use App\Services\CampaignManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ImportCodeHandler implements MessageHandlerInterface
{
    protected $campaignManager;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(CampaignManager $campaignManager, EntityManagerInterface $em)
    {
        $this->campaignManager = $campaignManager;
        $this->em = $em;
    }

    public function __invoke(ImportCode $importCode)
    {
        $campaign = $this->em->getRepository(Campaign::class)->find($importCode->getCampaign());
        $this->campaignManager->addCodes($campaign);
    }
}