<?php


namespace App\Listener;


use App\Entity\MedicinePieces;
use App\Entity\RequisitionItem;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class RequisitionItemListener implements EventSubscriber
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->updateRequisition($args);
    }


    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->updateRequisition($args);
    }

    protected function updateRequisition(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if (!$entity instanceof RequisitionItem) {
            return;
        }

        /** @var RequisitionItem $entity */

        $object = $this->entityManager->getRepository(MedicinePieces::class)->findOneBy(['medicine' => $entity->getMedicine(),'dose' => $entity->getQuantity()]);

        if($object && !$entity->getRequisition()->getIsCoupon()){
            $entity->setPieces($object->getPieces());
        }

        if($entity->getRequisition()->getIsCoupon()){
            $entity->setPieces($entity->getQuantity());
        }
    }
}