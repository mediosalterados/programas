<?php

namespace App\Listener;

use App\Entity\Requisition;
use App\Services\CycleManager;
use App\Services\RequisitionHelper;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class RequisitionListener implements EventSubscriber {

    /**
     * @var CycleManager
     */
    private $cycleManager;

    /**
     * @var RequisitionHelper
     */
    private $requisitionHelper;

    public function __construct(CycleManager $cycleManager, RequisitionHelper $requisitionHelper) {
        $this->cycleManager = $cycleManager;
        $this->requisitionHelper = $requisitionHelper;
    }

    /**
     * @inheritDoc
     */
    public function getSubscribedEvents() {
        return [
            Events::prePersist,
            Events::preUpdate
        ];
    }

    public function prePersist(LifecycleEventArgs $args) {
        $this->updateRequisition($args);
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $this->updateRequisition($args);
    }

    protected function updateRequisition(LifecycleEventArgs $args) {
        $entity = $args->getObject();
        if (!$entity instanceof Requisition) {
            return;
        }

        if ($entity->getStatus() == "renewed") {

            $fechaActual = new \DateTime('@' . strtotime('now'));

            if ($fechaActual >= $entity->getExpirationDate()) {
                $dateUpdate = $fechaActual;
            } else {
                $dateUpdate = $entity->getExpirationDate();
            }

            

            $entity->setExpirationDate($this->requisitionHelper->calculateExpirationDate($entity->getProgram(), $dateUpdate));
        }

        if (!$entity->getCycle()) {
            $entity->setCycle($this->cycleManager->getCurrentCycle());
        }

        if (!$entity->getStatus()) {
            $entity->setStatus('active');
        }

        if (!$entity->getExpirationDate()) {
            $entity->setExpirationDate($this->requisitionHelper->calculateExpirationDate($entity->getProgram(), $entity->getExpirationDate()));
        }
    }

}
