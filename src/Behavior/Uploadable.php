<?php


namespace App\Behavior;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

trait Uploadable
{
    /**
     * @ORM\Column(type="string", length=255,nullable=false)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="mst_uploads", fileNameProperty="image")
     * @Assert\File(maxSize="10M")
     * @var File
     */
    private $imageFile;

    /**
     * Set image
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    public function setImageFile(File $image = null) {
        $this->imageFile = $image;
        if ($image) {
            $this->updated = new \DateTime('now');
        }
    }

    public function getImageFile() {
        return $this->imageFile;
    }
}