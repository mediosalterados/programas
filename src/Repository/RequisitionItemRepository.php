<?php

namespace App\Repository;

use App\Entity\RequisitionItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RequisitionItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method RequisitionItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method RequisitionItem[]    findAll()
 * @method RequisitionItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequisitionItemRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, RequisitionItem::class);
    }

    // /**
    //  * @return RequisitionItem[] Returns an array of RequisitionItem objects
    //  */
    /*
      public function findByExampleField($value)
      {
      return $this->createQueryBuilder('r')
      ->andWhere('r.exampleField = :val')
      ->setParameter('val', $value)
      ->orderBy('r.id', 'ASC')
      ->setMaxResults(10)
      ->getQuery()
      ->getResult()
      ;
      }
     */

    /*
      public function findOneBySomeField($value): ?RequisitionItem
      {
      return $this->createQueryBuilder('r')
      ->andWhere('r.exampleField = :val')
      ->setParameter('val', $value)
      ->getQuery()
      ->getOneOrNullResult() 
      ;
      }
     */
    public function findByMedicine() {
        $q = $this->createQueryBuilder('r')
                ->andWhere('r.created BETWEEN :updatedI AND :updatedF')
                ->setParameter('updatedI', '2021-02-01 05:58:20.000000')
                ->setParameter('updatedF', '2021-02-27 05:58:20.000000');
        return $q->getQuery()->getResult();
    }

}
