<?php

namespace App\Repository;

use App\Entity\Requisition;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Requisition|null find($id, $lockMode = null, $lockVersion = null)
 * @method Requisition|null findOneBy(array $criteria, array $orderBy = null)
 * @method Requisition[]    findAll()
 * @method Requisition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequisitionRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Requisition::class);
    }

    public function getBaseQuery(User $user) {
        $q = $this->createQueryBuilder('sq')
                ->select('sq.id')
                ->andWhere('sq.user = :user')
                ->setParameter('user', $user);
        return $q;
    }

    public function findAllByMedicine() {
        $q = $this->createQueryBuilder('r')
                ->select('r.*')
                ->leftJoin('r.medicine', 'me')
                ->orderBy('me.name', 'ASC');
        return $q->getQuery()->getResult();
    }

    public function findByUser(User $user, $parameters) {
        $q = $this->createQueryBuilder('r')
                ->select('r')
                ->andWhere('r.user = :user')
                ->setParameter('user', $user)
                ->andWhere('r.status = :status_active OR r.status = :status_renewed')
                ->setParameter('status_renewed', 'renewed')
                ->setParameter('status_active', 'active');

        return $q->getQuery()->getResult();
    }

    public function findByUserToExpireTotal(User $user, $parameters) {
        $startDate = new \DateTime();
        $endDate = new \DateTime();
        $endDate->modify('+12 days');

        $q = $this->createQueryBuilder('r')
                ->select('r')
                ->andWhere('r.user = :user')
                ->setParameter('user', $user)
                ->andWhere('r.expirationDate BETWEEN :start AND :end')
                ->setParameter('start', $startDate)
                ->setParameter('end', $endDate)
                ->andWhere('r.status = :status_renewed')
                ->setParameter('status_renewed', 'to_renovate');


        return $q->getQuery()->getResult();
    }

    public function findBenefectarsByUser(User $user, $parameters = null) {
        $q = $this->createQueryBuilder('r')
                ->select('r.beneficiary')
                ->groupBy('r.beneficiary')
                ->andWhere('r.id IN (' . $this->getBaseQuery($user)->getDQL() . ')')
                ->setParameter('user', $user)
                ->orderBy('r.beneficiary', 'ASC');

        if ($parameters && isset($parameters['medico']) && $parameters['medico']) {
            $q->andWhere('r.doctor = :doctor')
                    ->setParameter('doctor', $parameters['medico']);
        }

        if ($parameters && isset($parameters['programa']) && $parameters['programa']) {
            $q->andWhere('r.program = :program')
                    ->setParameter('program', $parameters['programa']);
        }

        if ($parameters && isset($parameters['ciclos']) && $parameters['ciclos']) {
            $q->andWhere('r.cycle = :ciclo')
                    ->setParameter('ciclo', $parameters['ciclos']);
        }

        if ($parameters && isset($parameters['status']) && $parameters['status']) {
            $q->andWhere('r.status = :status')
                    ->setParameter('status', $parameters['status']);
        }

        if ($parameters && isset($parameters['isActive']) && $parameters['isActive']) {
            $q->andWhere('r.status = :status_active OR r.status = :status_renewed')
                    ->setParameter('status_renewed', 'renewed')
                    ->setParameter('status_active', 'active');
        }

        return $q->getQuery()
                        ->getResult();
    }

    public function findToExpire() {
        $startDate = new \DateTime();
        $endDate = new \DateTime();
        $endDate->modify('+5 days');
        $q = $this->createQueryBuilder('r')
                ->andWhere('r.expirationDate BETWEEN :start AND :end')
                ->setParameter('start', $startDate)
                ->setParameter('end', $endDate);

        return $q->getQuery()->getResult();
    }

    public function findToCanceled($parame = null) {
        $today = new \DateTime();
        $q = $this->createQueryBuilder('r')
                ->andWhere('r.expirationDate < :today')
                ->andWhere('r.status != :status')
                ->andWhere('r.cycle != :cycleA')
                ->setParameter('status', 'canceled')
                ->setParameter('today', $today)
                ->setParameter('cycleA', $parame);

        return $q->getQuery()->getResult();
    }

    public function findStatusByUser(User $user, $parameters = null) {
        $status = array('renewed', 'to_renovate', 'suspended', 'canceled');


        $q = $this->createQueryBuilder('r')
                ->select('r.status')
                ->groupBy('r.status')
                ->andWhere('r.id IN (' . $this->getBaseQuery($user)->getDQL() . ')')
                ->andWhere('r.status IN (:status)')
                ->setParameter('user', $user)
                ->setParameter('status', $status)
                ->orderBy('r.status', 'ASC');

        if ($parameters && isset($parameters['medico']) && $parameters['medico']) {
            $q->andWhere('r.doctor = :doctor')
                    ->setParameter('doctor', $parameters['medico']);
        }

        if ($parameters && isset($parameters['programa']) && $parameters['programa']) {
            $q->andWhere('r.program = :program')
                    ->setParameter('program', $parameters['programa']);
        }


        if ($parameters && isset($parameters['beneficiario']) && $parameters['beneficiario']) {
            $q->andWhere('r.beneficiary = :beneficiario')
                    ->setParameter('beneficiario', $parameters['beneficiario']);
        }

        if ($parameters && isset($parameters['ciclos']) && $parameters['ciclos']) {
            $q->andWhere('r.cycle = :ciclo')
                    ->setParameter('ciclo', $parameters['ciclos']);
        }

        return $q->getQuery()
                        ->getResult();
    }

    public function getDataDoctor() {
        $q = $this->createQueryBuilder('req')
                ->select('COUNT(req) as totales, dr.id as doctor_id, pr.id as program_id')
                ->leftJoin('req.doctor', 'dr')
                ->leftJoin('req.program', 'pr')
                ->andWhere('dr.isActive = :status')
                ->setParameter('status', 1)
                ->groupBy('req.program, req.doctor');

        return $q->getQuery()->execute();
    }

}
