<?php

namespace App\Repository;

use App\Entity\Cycle;
use App\Entity\Medicine;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Medicine|null find($id, $lockMode = null, $lockVersion = null)
 * @method Medicine|null findOneBy(array $criteria, array $orderBy = null)
 * @method Medicine[]    findAll()
 * @method Medicine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MedicineRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Medicine::class);
    }

    public function findByUser(User $user, Cycle $cycle = null, $orderpBy = true) {

        $q = $this->createQueryBuilder('m')
                ->select('m.id as medicine_id, m.name, SUM(r.pieces) as total')
                ->leftJoin('m.requisitions', 'r')
                ->leftJoin('r.requisition', 're')
                ->andWhere('re.user = :user')
                ->andWhere('re.status = :status_active OR re.status = :status_renewed OR re.status = :status_renovate')
                ->setParameter('status_renewed', 'renewed')
                ->setParameter('status_renovate', 'to_renovate')
                ->setParameter('status_active', 'active')
                ->setParameter('user', $user);

        if ($orderpBy) {

            $q->groupBy('m.id');
        }

        if ($cycle) {
            $q->andWhere('r.updated BETWEEN :updatedI AND :updatedF')
                    ->setParameter('updatedI', $cycle->getStartDate())
                    ->setParameter('updatedF', $cycle->getEndDate());
        }

//        dd($q->getQuery()->getResult());
        return $q->getQuery()->getResult();
    }

}
