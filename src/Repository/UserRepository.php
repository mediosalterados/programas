<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getBaseManagerQuery(User $user, $parameters = null){
        $q = $this->baseManagerQuery($user,$parameters);
        $q->select('r.id');
        return $q;
    }

    public function baseManagerQuery(User $user, $parameters = null){
        $q = $this->createQueryBuilder('u')
            ->select('u,r')
            ->leftJoin('u.requisitions', 'r')
            ->andWhere('u.distrito = :distrito')
            ->andWhere('u.id != :manager')
            ->setParameter('distrito', $user->getDistrito())
            ->setParameter('manager', $user);

        if ($parameters && isset($parameters['ciclos'])) {
            $q->andWhere('r.cycle = :cycle')
                ->setParameter('cycle', $parameters['ciclos']);
        }

        if ($parameters && isset($parameters['medico'])) {
            $q->andWhere('r.doctor = :doctor')
                ->setParameter('doctor', $parameters['medico']);
        }

        return $q;
    }

    public function findAllByDistrito(User $user, $parameters = null)
    {
        $q = $this->baseManagerQuery($user,$parameters);
        return $q->getQuery()
            ->getResult();

    }

}
