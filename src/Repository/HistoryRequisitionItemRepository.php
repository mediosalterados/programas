<?php

namespace App\Repository;

use App\Entity\HistoryRequisitionItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HistoryRequisitionItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method HistoryRequisitionItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method HistoryRequisitionItem[]    findAll()
 * @method HistoryRequisitionItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoryRequisitionItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HistoryRequisitionItem::class);
    }

    // /**
    //  * @return HistoryRequisitionItem[] Returns an array of HistoryRequisitionItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HistoryRequisitionItem
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
