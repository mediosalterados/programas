<?php

namespace App\Repository;

use App\Entity\Cycle;
use App\Entity\Doctor;
use App\Entity\Requisition;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Doctor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Doctor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Doctor[]    findAll()
 * @method Doctor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DoctorRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Doctor::class);
    }

    public function findByUser(User $user, $parameters, $toExpire = false, Cycle $cycle = null) {
        $q = $this->createQueryBuilder('d')
                ->select('d,r')
                ->leftJoin('d.requisitions', 'r')
                ->andWhere('r.user = :user')
                ->setParameter('user', $user);

        if ($toExpire) {
            $startDate = new \DateTime();
            $endDate = new \DateTime();
            $endDate->modify('+12 days');
            $q->andWhere('r.expirationDate BETWEEN :start AND :end')
                    ->setParameter('start', $startDate)
                    ->setParameter('end', $endDate)
                    ->andWhere('r.status = :status_to_renovate')
                    ->setParameter('status_to_renovate', 'to_renovate');
        }

        if ($cycle) {
            $q->andWhere('r.cycle = :cycle')
                    ->setParameter('cycle', $cycle);
        }

        if ($parameters) {
            if (isset($parameters['medico'])) {
                $q->andWhere('r.doctor = :doctor')
                        ->setParameter('doctor', $parameters['medico']);
            }

            if (isset($parameters['programa'])) {
                $q->andWhere('r.program = :program')
                        ->setParameter('program', $parameters['programa']);
            }

            if (isset($parameters['beneficiario'])) {
                $q->andWhere('r.beneficiary = :benefactor')
                        ->setParameter('benefactor', $parameters['beneficiario']);
            }

            if (isset($parameters['ciclos'])) {
                $q->andWhere('r.cycle = :ciclo')
                        ->setParameter('ciclo', $parameters['ciclos']);
            }

            if (isset($parameters['status'])) {
                $q->andWhere('r.status = :status')
                        ->setParameter('status', $parameters['status']);
            }

            if (isset($parameters['isHistoric']) && $parameters['isHistoric'] == true) {
//                dd("aqui");
                $q->andWhere('r.status = :status_suspended OR r.status = :status_to_renovate OR r.status = :status_renewed OR r.status = :status_canceled')
                        ->setParameter('status_to_renovate', 'to_renovate')
                        ->setParameter('status_renewed', 'renewed')
                        ->setParameter('status_suspended', 'suspended')
                        ->setParameter('status_canceled', 'canceled');
            }

            if (isset($parameters['isActive']) && $parameters['isActive'] == true) {
                $q->andWhere('r.status = :status_active OR r.status = :status_renewed')
                        ->setParameter('status_renewed', 'renewed')
                        ->setParameter('status_active', 'active');
            }
        }

        return $q->getQuery()->getResult();
    }

    public function findByMedicine(int $medicineId, User $user, Cycle $cycle = null) {
        $q = $this->createQueryBuilder('d')
                ->select('d.name,d.lastName, d.motherName, sum(i.pieces) as total, c.name as ciclo, r.beneficiary as beneficiarios ')
                ->leftJoin('d.requisitions', 'r')
                ->leftJoin('r.items', 'i')
                ->leftJoin('i.medicine', 'm')
                ->leftJoin('r.cycle', 'c')
                ->andWhere('r.user = :user')
                ->andWhere('m.id = :id')
                ->andWhere('r.status = :status_active OR r.status = :status_renewed OR r.status = :status_renovate')
                ->groupBy('r.doctor')
                ->setParameter('id', $medicineId)
                ->setParameter('user', $user)
                ->setParameter('status_renewed', 'renewed')
                ->setParameter('status_renovate', 'to_renovate')
                ->setParameter('status_active', 'active');

        if ($cycle) {
            $q->andWhere('r.updated BETWEEN :updatedI AND :updatedF')
                    ->setParameter('updatedI', $cycle->getStartDate())
                    ->setParameter('updatedF', $cycle->getEndDate());
        }

        return $q->getQuery()->getResult();
    }

    public function findDoctorsByManager(User $user, $parameters = null) {
        $q = $this->createQueryBuilder('rr')
            ->groupBy('rr.id')
            ->leftJoin('rr.requisitions', 're')
            ->andWhere('re.id IN (' . $this->getEntityManager()->getRepository(User::class)->getBaseManagerQuery($user)->getDQL() . ')')
            ->orderBy('rr.name', 'ASC')
            ->setParameter('distrito',$user->getDistrito())
            ->setParameter('manager', $user);

        if ($parameters && isset($parameters['ciclos']) && $parameters['ciclos']) {
            $q->andWhere('re.cycle = :ciclo')
                ->setParameter('ciclo', $parameters['ciclos']);
        }
        return $q;
    }


    public function findDoctorsByUser(User $user, $parameters = null) {

        $q = $this->createQueryBuilder('r')
                ->groupBy('r.id')
                ->leftJoin('r.requisitions', 're')
                ->andWhere('re.id IN (' . $this->getEntityManager()->getRepository(Requisition::class)->getBaseQuery($user)->getDQL() . ')')
                ->orderBy('r.name', 'ASC')
                ->setParameter('user', $user);

        if ($parameters && isset($parameters['programa']) && $parameters['programa']) {
            $q->andWhere('re.program = :program')
                    ->setParameter('program', $parameters['programa']);
        }

        if ($parameters && isset($parameters['beneficiario']) && $parameters['beneficiario']) {
            $q->andWhere('re.beneficiary = :beneficiary')
                    ->setParameter('beneficiary', $parameters['beneficiario']);
        }


        if ($parameters && isset($parameters['ciclos']) && $parameters['ciclos']) {
            $q->andWhere('re.cycle = :ciclo')
                    ->setParameter('ciclo', $parameters['ciclos']);
        }

        if ($parameters && isset($parameters['status']) && $parameters['status']) {
            $q->andWhere('re.status = :status')
                    ->setParameter('status', $parameters['status']);
        }

        return $q;
    }

}
