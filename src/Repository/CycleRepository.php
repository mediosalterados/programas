<?php

namespace App\Repository;

use App\Entity\Cycle;
use App\Entity\Requisition;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Cycle|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cycle|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cycle[]    findAll()
 * @method Cycle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CycleRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Cycle::class);
    }


    public function findCiclosByManager(User $user,$parameters = null){
        $q = $this->createQueryBuilder('c')
            ->groupBy('c.id')
            ->leftJoin('c.requisitions', 're')
            ->andWhere('re.id IN (' . $this->getEntityManager()->getRepository(User::class)->getBaseManagerQuery($user,$parameters)->getDQL() . ')')
            ->setParameter('manager', $user)
            ->setParameter('distrito', $user->getDistrito())
            ->orderBy('c.id', 'DESC');
        return $q;
    }

    public function findCiclosByUser(User $user, $parameters = null) {
        $q = $this->createQueryBuilder('r')
                ->groupBy('r.id')
                ->leftJoin('r.requisitions', 're')
                ->andWhere('re.id IN (' . $this->getEntityManager()->getRepository(Requisition::class)->getBaseQuery($user)->getDQL() . ')')
                ->setParameter('user', $user)
                ->orderBy('r.name', 'ASC');

        if ($parameters && isset($parameters['medico']) && $parameters['medico']) {
            $q->andWhere('re.doctor = :doctor')
                    ->setParameter('doctor', $parameters['medico']);
        }

        if ($parameters && isset($parameters['beneficiario']) && $parameters['beneficiario']) {
            $q->andWhere('re.beneficiary = :beneficiary')
                    ->setParameter('beneficiary', $parameters['beneficiario']);
        }

        if ($parameters && isset($parameters['programa']) && $parameters['programa']) {
            $q->andWhere('re.program = :programa')
                    ->setParameter('programa', $parameters['programa']);
        }

        return $q;
    }

    public function findCurrentCycle(\DateTime $date)
    {
        return $this->createQueryBuilder('c')
                        ->andWhere('c.startDate <= :date')
                        ->andWhere('c.endDate >= :date')
                        ->setParameter('date', $date)
                        ->getQuery()
                        ->getResult()
        ;
    }

}
