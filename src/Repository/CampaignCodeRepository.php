<?php

namespace App\Repository;

use App\Entity\CampaignCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CampaignCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method CampaignCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method CampaignCode[]    findAll()
 * @method CampaignCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CampaignCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CampaignCode::class);
    }

    // /**
    //  * @return CampaignCode[] Returns an array of CampaignCode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CampaignCode
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
