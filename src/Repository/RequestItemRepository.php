<?php

namespace App\Repository;

use App\Entity\RequisitionItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RequisitionItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method RequisitionItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method RequisitionItem[]    findAll()
 * @method RequisitionItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequestItemRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, RequisitionItem::class);
    }

    public function findAllByMedicine($cycleCurrent, $order = false ) {
        
        $q = $this->createQueryBuilder('rit')
                ->select('rit')
                ->Join('rit.medicine', 'me')
                ->Join('rit.requisition', 'r')
                ->Join('r.user', 'u')
                ->andWhere('r.cycle = :cycle')
                ->andWhere('r.status = :status_active OR r.status = :status_renewed ')
                ->setParameter('status_renewed', 'renewed')
                ->setParameter('status_active', 'active')
                ->setParameter('cycle', $cycleCurrent->getId());
//                ->andWhere('r.updated BETWEEN :updatedI AND :updatedF')
//                ->setParameter('updatedI', '2021-10-11 00:00:00.000000')
//                ->setParameter('updatedF', '2021-11-14')
        if ($order) {
            
            $q->orderBy('u.name', 'ASC');
        }else{
            $q->orderBy('rit.medicine', 'ASC');
        }
                
        return $q->getQuery()->getResult();
    }


    // /**
    //  * @return RequestItem[] Returns an array of RequestItem objects
    //  */
    /*
      public function findByExampleField($value)
      {
      return $this->createQueryBuilder('r')
      ->andWhere('r.exampleField = :val')
      ->setParameter('val', $value)
      ->orderBy('r.id', 'ASC')
      ->setMaxResults(10)
      ->getQuery()
      ->getResult()
      ;
      }
     */

    /*
      public function findOneBySomeField($value): ?RequestItem
      {
      return $this->createQueryBuilder('r')
      ->andWhere('r.exampleField = :val')
      ->setParameter('val', $value)
      ->getQuery()
      ->getOneOrNullResult()
      ;
      }
     */
}
