<?php

namespace App\Repository;

use App\Entity\Program;
use App\Entity\Requisition;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Program|null find($id, $lockMode = null, $lockVersion = null)
 * @method Program|null findOneBy(array $criteria, array $orderBy = null)
 * @method Program[]    findAll()
 * @method Program[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProgramRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Program::class);
    }

    public function findProgramsByUser(User $user,$parameters = null)
    {
        $q = $this->createQueryBuilder('r')
            ->groupBy('r.id')
            ->leftJoin('r.requisitions','re')
            ->andWhere('re.id IN ('.$this->getEntityManager()->getRepository(Requisition::class)->getBaseQuery($user)->getDQL().')')
            ->setParameter('user',$user)
            ->orderBy('r.name','ASC');

        if($parameters && isset($parameters['medico']) && $parameters['medico']){
            $q->andWhere('re.doctor = :doctor')
                ->setParameter('doctor',$parameters['medico']);
        }

        if($parameters && isset($parameters['beneficiario']) && $parameters['beneficiario']){
            $q->andWhere('re.beneficiary = :beneficiary')
                ->setParameter('beneficiary',$parameters['beneficiario']);
        }

        if ($parameters && isset($parameters['ciclos']) && $parameters['ciclos']) {
            $q->andWhere('re.cycle = :ciclo')
                ->setParameter('ciclo', $parameters['ciclos']);
        }

        if ($parameters && isset($parameters['status']) && $parameters['status']) {
            $q->andWhere('re.status = :status')
                ->setParameter('status', $parameters['status']);
        }

        return $q;
    }
}
