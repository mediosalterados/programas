<?php

namespace App\Repository;

use App\Entity\Renewal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Renewal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Renewal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Renewal[]    findAll()
 * @method Renewal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RenewalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Renewal::class);
    }

    // /**
    //  * @return Renewal[] Returns an array of Renewal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Renewal
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
