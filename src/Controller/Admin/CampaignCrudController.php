<?php

namespace App\Controller\Admin;

use App\Entity\Campaign;
use App\Entity\CampaignCode;
use PhpZip\Exception\ZipException;
use PhpZip\ZipFile;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;

class CampaignCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return Campaign::class;
    }

    public function configureFields(string $pageName): iterable {
        return [
        IdField::new('id')->onlyOnIndex(),
        TextField::new('file'),
        DateTimeField::new('created'),
        AssociationField::new('codes')
        ];
    }

    public function configureActions(Actions $actions): Actions {
        $actions->disable(Action::NEW, Action::DELETE, Action::EDIT);

//        dd($actions);

        $detailsAction = Action::new('Details', '')
        ->setIcon('fas fa-clone')
                ->setLabel('detalle')
                ->linkToCrudAction('detailsAction');


        $dwAction = Action::new('Dw', '')
        ->setIcon('fas fa-clone')
                ->setLabel('Dw')
                ->linkToCrudAction('zip');


        return $actions
                        ->add(Crud::PAGE_INDEX, $detailsAction)
                        ->add(Crud::PAGE_INDEX, $dwAction);
    }

    public function detailsAction(AdminContext $context) {

        $id = $context->getRequest()->query->get('entityId');

        $repository = $this->getDoctrine()->getRepository(CampaignCode::class);
        $coupons = $repository->findBy([
            'campaign' => $id,
        ]);
        return $this->render('coupon/per_campaign.html.twig', [
                    'coupons' => $coupons,
        ]);
    }

    public function zip(AdminContext $context) {
        $id = $context->getRequest()->query->get('entityId');
        $zipFile = new ZipFile();
        try {
            $zipFile->addDir(sprintf('coupons/c%s', $id))->outputAsAttachment(sprintf('coupons-c%s.zip', $id));
        } catch (ZipException $e) {
            
        } finally {
            $zipFile->close();
        }
    }

}
