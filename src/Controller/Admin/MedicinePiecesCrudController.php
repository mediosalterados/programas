<?php

namespace App\Controller\Admin;

use App\Entity\MedicinePieces;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;

class MedicinePiecesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return MedicinePieces::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            AssociationField::new('medicine'),
            NumberField::new('dose'),
            NumberField::new('pieces'),
        ];
    }
}
