<?php

namespace App\Controller\Admin;

use App\Entity\Medicine;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class MedicineCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return Medicine::class;
    }

    public function configureFields(string $pageName): iterable {
        return [
        IdField::new('id')->onlyOnIndex(),
        TextField::new('name'),
            TextField::new('sku'),
        NumberField::new('maximumPerDose'),
            NumberField::new('maximumPerCoupon'),
        ArrayField::new('lineas')->onlyOnForms(),
        ];
    }

}
