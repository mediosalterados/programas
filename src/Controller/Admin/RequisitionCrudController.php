<?php

namespace App\Controller\Admin;

use App\Entity\Requisition;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;

class RequisitionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Requisition::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        
        return [
            TextField::new('name'),
            DateTimeField::new('start_date'),
            DateTimeField::new('end_date'),
        ];
    }
    
}
