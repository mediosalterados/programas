<?php

namespace App\Controller\Admin;

use App\Entity\Brand;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

class BrandCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return Brand::class;
    }

    public function configureFields(string $pageName): iterable {




        $imageFile = ImageField::new('frontImageFile')->setFormType(VichImageType::class);
        $image = ImageField::new('frontImage')->setBasePath('/images/cupons');
        $imageFileB = ImageField::new('backImageFile')->setFormType(VichImageType::class);
        $imageB = ImageField::new('backImage')->setBasePath('/images/cupons');

        $fields = [
        TextField::new('name'),
        ImageField::new('frontImageFile')->setFormType(VichImageType::class)->onlyOnForms(),
        ImageField::new('backImageFile')->setFormType(VichImageType::class)->onlyOnForms(),
        ];

        if ($pageName == Crud::PAGE_INDEX || $pageName == Crud::PAGE_DETAIL) {
            $fields[] = $image;
            $fields[] = $imageB;
        } else {
            $fields[] = $imageFile;
            $fields[] = $imageFileB;
            
        }
        return $fields;

    }

}
