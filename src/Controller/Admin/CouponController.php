<?php

namespace App\Controller\Admin;

use App\Entity\Campaign;
use App\Form\CampaignType;
use App\Message\ImportCode;
use App\Services\ZipGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/coupon")
 */
class CouponController extends AbstractController
{
    /**
     * @Route("/", name="coupon_index")
     */
    public function index(Request $request, EntityManagerInterface $em, MessageBusInterface $messageBus)
    {
        $campaign = new Campaign();
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($campaign);
            $em->flush();
            $message = new ImportCode($campaign->getId());
            $messageBus->dispatch($message);
            return $this->redirect($this->generateUrl('coupon_show', ['id' => $campaign->getId(), 'eaContext' => $request->query->get('eaContext')]));
        }

        return $this->render('coupon/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/campaign/{id}", name="coupon_show")
     */
    public function show(Campaign $campaign, Filesystem $filesystem, Request $request, $pdfPath)
    {
        $finder = new Finder();
        $folder = sprintf('%sc%s', $pdfPath, $campaign->getId());
        $files = 0;
        $ready = false;
        if ($filesystem->exists($folder)) {
            $files = $finder->files()->in($folder)->name('/\.pdf$/')->count();
            $ready = $campaign->getCodes()->count() == $files ? true : false;
        }

        return $this->render('coupon/show.html.twig', [
            'entity' => $campaign,
            'files' => $files,
            'isReady' => $ready
        ]);
    }

    /**
     * @Route("/campaign/zip/{id}", name="coupon_zip")
     */
    public function zip(Campaign $campaign, ZipGenerator $zipGenerator)
    {
        $zipGenerator->zipCoupons($campaign);
    }
}
