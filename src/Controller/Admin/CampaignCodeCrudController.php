<?php

namespace App\Controller\Admin;

use App\Entity\CampaignCode;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */
class CampaignCodeCrudController extends AbstractCrudController {

    /**
     * @Route("/test/{id}", name="campaign_id")
     * 
     */
    public function test(Request $request, string $id): Response {
        $repository = $this->getDoctrine()->getRepository(CampaignCode::class);
        $karim = $repository->findBy([
            'campaign' => $id,
        ]);

        return $this->render('coupon/per_campaign.html.twig');
    }

    public static function getEntityFqcn(): string {
        return CampaignCode::class;
    }

    public function configureFields(string $pageName): iterable {
        return [
        TextField::new('product'),
        TextField::new('code'),
        TextField::new('campaign'),
        TextField::new('barcode'),
        ];
    }

}
