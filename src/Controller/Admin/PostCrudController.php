<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

class PostCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return Post::class;
    }

    public function configureCrud(Crud $crud): Crud {
        return $crud->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
    }

    public function configureFields(string $pageName): iterable {
        return [
        TextField::new('title', 'Titulo'),
        TextField::new('resume', 'Resumen'),
        TextField::new('content', 'Contenido')->setFormType(CKEditorType::class)->onlyOnForms(),
        TextareaField::new('imageFile')->setFormType(VichImageType::class)->onlyOnForms(),
        TextareaField::new('bannerFile')->setFormType(VichImageType::class)->onlyOnForms(),
        Field::new('prominent'),
        ];
    }

}
