<?php

namespace App\Controller\Admin;

use App\Entity\Brand;
use App\Entity\Campaign;
use App\Entity\Medicine;
use App\Entity\MedicinePieces;
use App\Entity\Program;
use App\Entity\Cycle;
use App\Entity\Post;
use App\Entity\User;
use App\Entity\Doctor;
use App\Entity\CampaignCode;
use App\Entity\Requisition;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     * @return Response
     */
    public function index(): Response
    {
        
         $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

//        return $this->redirect($routeBuilder->setController(CycleCrudController::class)->generateUrl());
        
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Silanes Programas');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToCrud('Ciclos', 'far fa-newspaper', Cycle::class);
        yield MenuItem::linkToCrud('Programas', 'fas fa-comments', Program::class);
        yield MenuItem::linkToCrud('Solicitudes', 'fa fa-keyboard-o', Requisition::class);
        yield MenuItem::linkToCrud('Medicamentos', 'fa fa-briefcase', Medicine::class);
        yield MenuItem::linkToCrud('Noticias', 'fa fa-user', Post::class);
        yield MenuItem::linkToCrud('Representantes', 'fa fa-file-text-o', User::class);
        yield MenuItem::linkToCrud('Medicos', 'fa fa-info-circle', Doctor::class);
//        yield MenuItem::linktoRoute('Crear cupones','fa fa-tags','coupon_index');
//        yield MenuItem::linkToCrud('Marcas cupones','fa fa-registered',Brand::class);
//        yield MenuItem::linkToCrud('Cupones creados','fa fa-cloud-download-alt',Campaign::class);
        yield MenuItem::linkToCrud('medicinas piezas','fa fa-cloud-download-alt',MedicinePieces::class);


    }
}

