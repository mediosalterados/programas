<?php

namespace App\Controller\Admin;

use App\Entity\Cycle;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

class CycleCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return Cycle::class;
    }

    public function configureFields(string $pageName): iterable {
        return [
        TextField::new('name'),
        DateField::new('start_date')->setFormat('Y-MM-dd')->renderAsNativeWidget(),
        DateField::new('end_date')->setFormat('Y-MM-dd')->renderAsNativeWidget(),
        Field::new('enabled'),
        ];
    }

}
