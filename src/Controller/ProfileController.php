<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\UserPasswordType;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;

class ProfileController extends AbstractController {

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {

        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function index(Request $request): Response {

        $passwordForm = $this->createForm(UserPasswordType::class, $this->getUser());

        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $flashBag->add('success', 'Perfil actualizado exitosamente');
                $em->persist($this->getUser());
                $em->flush();

                return $this->redirectToRoute('wip_profile');
            }
        }


        $user = $this->getUser();
        return $this->render('profile/index.html.twig', [
                    'passwordForm' => $passwordForm->createView(),
                    'user' => $user,
        ]);
    }

    /**
     * @Route("/set/password",name="wip_profile_password", methods={"post"})
     */
    public function changePassword(Request $request, FlashBagInterface $flashBag, UserPasswordEncoderInterface $encoder) {
        $user = $this->getUser();
        $form = $this->createForm(UserPasswordType::class, $user);

        $em = $this->getDoctrine()->getManager();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $this->getUser();
            $vpass = $form->get('oldPassword')->getData();

            if ($this->passwordEncoder->isPasswordValid($user, $vpass)) {
                $plainPassword = $form->get('plainPassword')->getData();
                $password = $encoder->encodePassword($user, $plainPassword);
                $user->setPassword($password);
                $em->persist($user);
                $em->flush();

                $flashBag->add('success', 'Tu contraseña se actualizo correctamente');
            } else {
                $flashBag->add('danger', 'Contraseña no valida');
                return $this->redirectToRoute('profile');
            }
        } else {
            $flashBag->add('danger', 'Tu contraseña no se actualizo, intenta nuevamente.');
        }

        return $this->redirectToRoute('profile');
    }

}
