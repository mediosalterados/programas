<?php

namespace App\Controller;
use App\Entity\Post;
use App\Repository\PostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController {

    /**
     * @Route("/noticias", name="post_index")
     */
    public function index(PaginatorInterface $paginator, Request $request, PostRepository $postRepository): Response {

        $slider = $postRepository->findBy(array('prominent' => 1), array('created' => 'DESC'));
        $entities = $postRepository->findAll();
        $pagination = $paginator->paginate(
                $entities, $request->query->getInt('page', 1), 10
        );
        return $this->render('post/index.html.twig', [
                    'pagination' => $pagination,
                    'title' => 'Noticias',
                    'slider' => $slider,
        ]);
    }

    /**
     * @Route("/noticias/{id}", name="post_view")
     */
    public function reedem(Post $post) {
        
//        dd($post);

        return $this->render('post/post.html.twig', [
                    "post" => $post,
        ]);
    }

}
