<?php

namespace App\Controller;

use App\Entity\Requisition;
use App\Form\RequisitionType;
use App\Repository\PostRepository;
use App\Services\DataHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController {

    /**
     * @Route("/", name="homepage")
     */
    public function index(PostRepository $postRepository, DataHelper $dataHelper): Response {
        if ($this->getUser()->getRecurrent() != 1) {
            return $this->redirectToRoute('gracias');
        }
        
        $slider = $postRepository->findBy(array('prominent' => 1), array('created' => 'DESC'));
        return $this->render('default/index.html.twig', [
                    'slider' => $slider,
                    'shortcutsData' => $dataHelper->getShortCutsData()
        ]);
    }

    /**
     * @Route("/gracias",name="gracias")
     */
    public function gracias() {
        return $this->render('registration/gracias.html.twig');
    }

    /**
     * @Route("/acepto",name="acepto")
     */
    public function acepto() {
        $em = $this->getDoctrine()->getManager();
        $user_login = $this->getUser();
        $user_login->setRecurrent(1);
        $em->persist($user_login);
        $em->flush();
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/aviso",name="aviso")
     */
    public function aviso() {
        return $this->render('terminos/aviso.html.twig');
    }

    /**
     * @Route("/terminos",name="terminos")
     */
    public function terminos() {
        return $this->render('terminos/terminos.html.twig');
    }

}
