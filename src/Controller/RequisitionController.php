<?php

namespace App\Controller;

use App\Entity\Requisition;
use App\Entity\Renewal;
use App\Entity\MedicinePieces;
use App\Entity\HistoryRequisitionItem;
use App\Repository\HistoryRequisitionItemRepository;
use App\Repository\RenewalRepository;
use App\Form\DataFilterType;
use App\Form\DataHistoricFilterType;
use App\Form\MedicineFilterType;
use App\Form\RequisitionEditType;
use App\Form\RequisitionType;
use App\Services\CycleManager;
use App\Services\DataHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Workflow\Registry;

/**
 * @Route("/requisition")
 */
class RequisitionController extends AbstractController
{

    private $dataHelper;

    public function __construct(DataHelper $dataHelper)
    {
        $this->dataHelper = $dataHelper;
    }

    public function renderForm(Request $request, $type = DataFilterType::class)
    {
        $form = $this->createForm($type);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $parameters = $form->getData();
            $this->dataHelper->setParameters($parameters);
        }

        return $form->createView();
    }

    /**
     * @Route("/", name="requisition_index")
     */
    public function index(Request $request, CycleManager $cycleManager, DataHelper $dataHelper): Response
    {
        if ($cycleManager->getCurrentCycle()) {
            $cycleManager->setUseCurrentCycle(true);
            $view = $request->isXmlHttpRequest() ? 'requisition/_content_list.html.twig' : 'requisition/index.html.twig';
            return $this->render($view, [
                'renew_available' => false,
                'form' => $this->renderForm($request),
                'entities' => $this->dataHelper->getActiveRequisitions(),
                'title' => 'Solicitudes Activas'
            ]);
        } else {
            return $this->redirectToRoute('homepage');
        }
    }

    /**
     * @Route("/renew", name="requisition_renew")
     */
    public function renew(Request $request, CycleManager $cycleManager): Response
    {

        if ($cycleManager->getCurrentCycle()) {
            $view = $request->isXmlHttpRequest() ? 'requisition/_content_list.html.twig' : 'requisition/index.html.twig';
            return $this->render($view, [
                'renew_available' => true,
                'form' => $this->renderForm($request),
                'entities' => $this->dataHelper->getToExpireRequisitions(),
                'title' => 'Renovar / Continuar'
            ]);
        } else {
            return $this->redirectToRoute('homepage');
        }
    }

    /**
     * @Route("/history", name="requisition_history")
     */
    public function history(Request $request, CycleManager $cycleManager): Response
    {

        if ($cycleManager->getCurrentCycle()) {
            $view = $request->isXmlHttpRequest() ? 'requisition/_content_list.html.twig' : 'requisition/index.html.twig';
            return $this->render($view, [
                'renew_available' => false,
                'form' => $this->renderForm($request, DataHistoricFilterType::class),
                'entities' => $this->dataHelper->getAllRequisitions(),
            ]);
        } else {
            return $this->redirectToRoute('homepage');
        }
    }

    /**
     * @Route("/medicine", name="requisition_medicine")
     */
    public function medicine(Request $request, DataHelper $dataHelper): Response
    {
        return $this->render('requisition/medicines.html.twig', [
            'form' => $this->renderForm($request, MedicineFilterType::class),
            'entities' => $this->dataHelper->getActiveRequisitionsByMedicine(),
            'title' => 'Solicitudes Activas',
            'shortcutsData' => $dataHelper->getShortCutsData()
        ]);
    }

    /**
     * @Route("/suspend/{id}", name="requisition_suspend")
     */
    public function suspend(Registry $registry, Request $request, EntityManagerInterface $entityManager, Requisition $requisition): Response
    {

        $workflow = $registry->get($requisition);
        if ($workflow->can($requisition, 'to_suspended') && $requisition->getUser() == $this->getUser()) {
            $workflow->apply($requisition, 'to_suspended');
            $entityManager->persist($requisition);
            $entityManager->flush();
            $this->addFlash('info', "Requisición suspendida");
        }

        $referer = $request->headers->get('referer');
        $url = $referer ? $referer : $this->generateUrl('requisition_index');
        return $this->redirect($url);
    }

    /**
     * @Route("/detalle/{id}s",name="detalles")
     */
    public function detalle(Registry $registry, Request $request, EntityManagerInterface $entityManager, Requisition $requisition): Response
    {

        return $this->render('requisition/detalles.html.twig', [
            'requisition' => $requisition,
        ]);
    }

    /**
     * @Route("/active/{id}", name="requisition_to_active")
     */
    public function toActive(FlashBagInterface $flashBag, Registry $registry, Request $request, EntityManagerInterface $entityManager, Requisition $requisition): Response
    {
        $workflow = $registry->get($requisition);
        $referer = $request->headers->get('referer');
        $url = $referer ? $referer : $this->generateUrl('requisition_index');

        if (!$workflow->can($requisition, 'to_activate') || !$requisition->getUser() == $this->getUser()) {
            return $this->redirect($url);
        }

        $workflow->apply($requisition, 'to_activate');

        $entityManager->persist($requisition);
        $entityManager->flush();
        $flashBag->add('nfancy', '');
        return $this->redirectToRoute('requisition_index');
    }

    /**
     * @Route("/renew/{id}", name="requisition_to_renew")
     */
    public function torenew(FlashBagInterface $flashBag, Registry $registry, Request $request, EntityManagerInterface $entityManager, Requisition $requisition, RenewalRepository $renewalRepository, CycleManager $cycleManager, DataHelper $dataHelper): Response {
        $workflow = $registry->get($requisition);
        $referer = $request->headers->get('referer');
        $url = $referer ? $referer : $this->generateUrl('requisition_index');

        $cycleCurrent = $dataHelper->processCycleData();
        
        $renewal = $renewalRepository->findBy(['requisition' => $requisition, 'cycle' => $cycleCurrent['cycle']]);

        if (!empty($renewal)) {
            $url = $this->generateUrl('requisition_renew');
            $flashBag->add('alreadyrenewed', '');
            return $this->redirect($url);
        }

        if (!$workflow->can($requisition, 'to_renewed') || !$requisition->getUser() == $this->getUser()) {
            return $this->redirect($url);
        }
        $form = $this->createForm(RequisitionEditType::class, $requisition);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $workflow->apply($requisition, 'to_renewed');
            foreach ($requisition->getItems() as $item) {
                $item->setUpdated(new \DateTime());
            }
            $renewal = new Renewal();
            $renewal->setRequisition($requisition);
            $renewal->setCycle($cycleCurrent['cycle']);
            $renewal->setRenovationDate(new \DateTime());
            $entityManager->persist($renewal);
            $entityManager->persist($requisition);
            $this->historyItemAdd($requisition, $entityManager, $dataHelper);
            $entityManager->flush();
            $flashBag->add('nfancy', '');
            return $this->redirectToRoute('requisition_index');
        }

        return $this->render('requisition/edit.html.twig', [
            'requisition' => $requisition,
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/new", name="requisition_new")
     */
    public function new(FlashBagInterface $flashBag, Request $request, EntityManagerInterface $entityManager, DataHelper $dataHelper ): Response
    {
        $requisition = new Requisition();
        $form = $this->createForm(RequisitionType::class, $requisition);
        $form->handleRequest($request);
        $cycleCurrent = $dataHelper->processCycleData();
        if ($form->isSubmitted() && $form->isValid()) {
            if (count($form->get("items")->getData()) < 1) {
                $flashBag->add('warning', ' Es necesario agregar al menos un medicamento.');
                return $this->redirectToRoute('requisition_new');
            }
            
            foreach ($form->get("items") as $item) {
            $medicamentos[] = $item->get('medicine')->getData()->getName();
             }
             if (count($medicamentos) !== count(array_unique($medicamentos))) {
                 $flashBag->add('warning', 'No es posible agregar el mismo medicamento');
                return $this->redirectToRoute('requisition_new');
             }
            $initials = $form->get("first")->getData() . $form->get("second")->getData() . $form->get("third")->getData() . $form->get("quarter")->getData();
            $requisition->setUser($this->getUser());
            $requisition->setBeneficiary($initials);
            $entityManager->persist($requisition);
            $this->historyItemAdd($requisition, $entityManager, $dataHelper);
            
            $entityManager->flush();
            $flashBag->add('nfancy', '');
            return $this->redirectToRoute('requisition_index');
        }

        return $this->render('requisition/new.html.twig', [
            'title' => 'Nueva Solicitud',
            'form' => $form->createView()
        ]);
    }
    
     public function historyItemAdd($requisition, EntityManagerInterface $entityManager, DataHelper $dataHelper ) {
        
         $cycleCurrent = $dataHelper->processCycleData();
        foreach ($requisition->getItems() as $item) {
            $historyRequisitionItem = new HistoryRequisitionItem();
            $historyRequisitionItem->setRequisition($item->getRequisition());
            $historyRequisitionItem->setMedicine($item->getMedicine());
            $object = $entityManager->getRepository(MedicinePieces::class)->findOneBy(['medicine' => $item->getMedicine(),'dose' => $item->getQuantity()]);
            $historyRequisitionItem->setQuantity($object->getPieces());
            $historyRequisitionItem->setCurrentCycle($cycleCurrent['cycle']);
            $entityManager->persist($historyRequisitionItem);
            }
            return $requisition;
    }

}
