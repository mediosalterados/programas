<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Requisition;
use App\Repository\RequisitionRepository;
use App\Entity\Medicine;
use App\Entity\Cycle;
use App\Repository\MedicineRepository;
use App\Repository\RequisitionItemRepository;
use App\Entity\RequisitionItem;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class XlsController extends AbstractController {

    private $requisitionRepository;
    private $medicineRepository;

    public function __construct(MedicineRepository $medicineRepository) {
        $this->medicineRepository = $medicineRepository;
    }

    /**
     * @Route("/xls", name="xls")
     */
    public function index(): Response {
        return $this->render('xls/index.html.twig', [
                    'controller_name' => 'XlsController',
        ]);
    }

    /**
     * @Route("/export", name="export")
     */
    public function exportAction(Request $request, RequisitionRepository $requisitionRepository) {

        $repository = $this->getDoctrine()->getRepository(Cycle::class);
        $cycleR = $repository->findBy([
            'id' => '13',
        ]);
        $statusPaciente = array('renewed' => 'Activo', 'to_renovate' => 'Activo', 'suspended' => 'Inactivo', 'active' => 'Activo', 'canceled' => 'Inactivo');
        $statusRequisition = array('renewed' => 'Renovado', 'to_renovate' => 'Por renovar', 'suspended' => 'Suspendida', 'active' => 'Nuevo', 'canceled' => 'Cancelada');
        $em = $this->getDoctrine()->getManager();
        $requisitions = $this->getDoctrine()->getRepository(RequisitionItem::class)->findAllByMedicine($cycleR[0], $order = false);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("My First Worksheet");
        $writer = new Xlsx($spreadsheet);
        $headers = array('Programa', 'Iniciales Paciente', 'Estatus de paciente', 'Ruta', 'Linea', 'Distrito', 'Nombre Representante', 'Num_Empleado',
            'Nombre Médico', 'Id. Tratamiento', 'Estatus de Solicitud', 'Cod Med CRM', 'Especialidad', 'Fecha Alta de paciente',
            'Fecha actualizacion', 'Fecha expiración', 'Medicamento', 'SKU', 'Dosis', 'Suma de Piezas', 'Ciclo');
        $cols = array();
        $letter = 'A';
        while ($letter !== 'V') {
            $cols[] = $letter++;
        }

        foreach ($headers as $i => $header) {
            $sheet->setCellValue($cols[$i] . '1', $headers[$i]);
        }
        $index = 2;


        foreach ($requisitions as $requisition) {
            $data = null;


            $entities = $this->medicineRepository->findByUser($requisition->getRequisition()->getUser(), $cycleR[0], false);
            ////////Modificar creacion para que sea automatioco y actualizar todos los anteriores con la misma formula 
            if ($requisition->getRequisition()->getProgram()->getId() == 3) {
                $piezas = ($requisition->getPieces() * 3);
            } else {
                $piezas = $requisition->getPieces();
            }
            //////////


            if ($statusPaciente[$requisition->getRequisition()->getStatus()] == 'Activo' AND $statusRequisition[$requisition->getRequisition()->getStatus()] == 'Renovado') {
                $paciente = "Renovado";
            } else {
                $paciente = $statusPaciente[$requisition->getRequisition()->getStatus()];
            }


            if ($requisition->getMedicine()) {
                if ($requisition->getMedicine()->getId() == 15 AND $requisition->getRequisition()->getProgram()->getId() == 3) {
                    
                } else {
                    $data = array(
                        $requisition->getRequisition()->getProgram()->getName(),
                        $requisition->getRequisition()->getBeneficiary(),
                        $paciente,
                        $requisition->getRequisition()->getUser()->getRuta(),
                        $requisition->getRequisition()->getUser()->getLinea(),
                        $requisition->getRequisition()->getUser()->getDistrito(),
                        $requisition->getRequisition()->getUser()->getName(),
                        $requisition->getRequisition()->getUser()->getClientNumber(),
                        $requisition->getRequisition()->getDoctor()->getName() . " " . $requisition->getRequisition()->getDoctor()->getLastName() . " " . $requisition->getRequisition()->getDoctor()->getMotherName(),
                        $requisition->getRequisition()->getClave(),
                        $statusRequisition[$requisition->getRequisition()->getStatus()],
                        $requisition->getRequisition()->getDoctor()->getCodCrm(),
                        $requisition->getRequisition()->getDoctor()->getSpeciality(),
                        $requisition->getRequisition()->getCreated(),
                        $requisition->getRequisition()->getUpdated(),
                        $requisition->getRequisition()->getExpirationDate(),
                        $requisition->getMedicine()->getName(),
                        $requisition->getMedicine()->getSku(),
                        $requisition->getQuantity(),
                        $piezas,
                        $requisition->getRequisition()->getCycle()->getName(),
//                        $entities && isset($entities[0]['total']) ? $entities[0]['total'] : 0,
                    );
                }
            }
            foreach ($headers as $i => $header) {
                if ($data && isset($data[$i])) {
                    $sheet->setCellValue($cols[$i] . $index, $data[$i]);
                }
            }
            if ($data) {
                $index++;
            }
        }
        $fileName = 'reporte.xlsx';

        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

}
