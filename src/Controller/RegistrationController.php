<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use App\Security\LoginFormAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class RegistrationController extends AbstractController {

    /**
     * @Route("/registro", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator, UserRepository $userRepository, MailerInterface $mailer): Response {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $existingUser = $userRepository->findOneBy(array('email' => $user->getEmail()));

            if ($existingUser && $existingUser->getPassword() == "INACTIVO" && $existingUser->getIsActive() == true) {
                $existingUser->setPassword(
                        $passwordEncoder->encodePassword(
                                $existingUser, $form->get('plainPassword')->getData()
                        )
                );

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($existingUser);
                $entityManager->flush();

                $email = (new TemplatedEmail())
                        ->from(new Address('intranet@silanes.com.mx', 'Gracias por registrarte a Portal Programas Silanes'))
                        ->to($user->getEmail())
//                        ->to("mariokarim1109@gmail.com")
                        ->subject('Gracias por registrarte a Portal Programas Silanes')
                        ->htmlTemplate('mail/gracias.html.twig')
                        ->context([
                    'pass' => $form->get('plainPassword')->getData(),
                    'user' => $existingUser,
                        ])
                ;

                $mailer->send($email);


                return $guardHandler->authenticateUserAndHandleSuccess(
                                $existingUser, $request, $authenticator, 'main'
                );
            } else {
                $form->get('email')->addError(new FormError('Correo no válido'));
                return $this->render('registration/register.html.twig', [
                            'registrationForm' => $form->createView(),
                ]);
            }
            return $this->render('registration/gracias.html.twig');
        }

        return $this->render('registration/register.html.twig', [
                    'registrationForm' => $form->createView(),
        ]);
    }

}
