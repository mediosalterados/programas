<?php

namespace App\Controller;

use App\Entity\Cycle;
use App\Entity\RequisitionItem;
use App\Repository\DoctorRepository;
use App\Repository\MedicineRepository;
use App\Repository\RequisitionRepository;
use App\Services\DataHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class ApiController extends AbstractController
{

    private $medicineRepository;

    public function __construct(MedicineRepository $medicineRepository)
    {
        $this->medicineRepository = $medicineRepository;
    }

    /**
     * @Route("/", name="api")
     */
    public function index(Request $request, RequisitionRepository $requisitionRepository, DataHelper $dataHelper): Response
    {

        $cycleCurrent = $dataHelper->processCycleData();
        $em = $this->getDoctrine()->getManager();
        $requisitions = $this->getDoctrine()->getRepository(RequisitionItem::class)->findAllByMedicine($cycleCurrent["cycle"], $order = true);
        $statusPaciente = array('renewed' => 'Activo', 'to_renovate' => 'Activo', 'suspended' => 'Inactivo', 'active' => 'Activo', 'canceled' => 'Inactivo');
        $statusRequisition = array('renewed' => 'Renovado', 'to_renovate' => 'Por renovar', 'suspended' => 'Suspendida', 'active' => 'Nuevo', 'canceled' => 'Cancelada');


        foreach ($requisitions as $requisition) {
            $data = null;

            $repository = $this->getDoctrine()->getRepository(Cycle::class);
            ////////Modificar creacion para que sea automatioco y actualizar todos los anteriores con la misma formula 
            if ($requisition->getRequisition()->getProgram()->getId() == 3) {
                $piezas = ($requisition->getPieces() * 3);
            } else {
                $piezas = $requisition->getPieces();
            }
            //////////

            if ($statusPaciente[$requisition->getRequisition()->getStatus()] == 'Activo' AND $statusRequisition[$requisition->getRequisition()->getStatus()] == 'Renovado') {
                $paciente = "Renovado";
            } else {
                $paciente = $statusPaciente[$requisition->getRequisition()->getStatus()];
            }


            if ($requisition->getMedicine()) {
                if ($requisition->getMedicine()->getId() == 15 AND $requisition->getRequisition()->getProgram()->getId() == 3) {

                } else {
                    $total[] = $data = array(
                        'Programa' => $requisition->getRequisition()->getProgram()->getName(),
                        'Iniciales Paciente' => $requisition->getRequisition()->getBeneficiary(),
                        'Estatus de paciente' => $paciente,
                        'Ruta' => $requisition->getRequisition()->getUser()->getRuta(),
                        'Linea' => $requisition->getRequisition()->getUser()->getLinea(),
                        'Distrito' => $requisition->getRequisition()->getUser()->getDistrito(),
                        'Nombre Representante' => $requisition->getRequisition()->getUser()->getName(),
                        'Num_Empleado' => $requisition->getRequisition()->getUser()->getClientNumber(),
                        'Nombre Médico' => $requisition->getRequisition()->getDoctor()->getName() . " " . $requisition->getRequisition()->getDoctor()->getLastName() . " " . $requisition->getRequisition()->getDoctor()->getMotherName(),
                        'Id. Tratamiento' => $requisition->getRequisition()->getClave(),
                        'Estatus de Solicitud' => $statusRequisition[$requisition->getRequisition()->getStatus()],
                        'Cod Med CRM' => $requisition->getRequisition()->getDoctor()->getCodCrm(),
                        'Especialidad' => $requisition->getRequisition()->getDoctor()->getSpeciality(),
                        'Fecha Alta de paciente' => $requisition->getRequisition()->getCreated(),
                        'Fecha actualizacion' => $requisition->getRequisition()->getUpdated(),
                        'Fecha expiración' => $requisition->getRequisition()->getExpirationDate(),
                        'Medicamento' => $requisition->getMedicine()->getName(),
                        'SKU' => $requisition->getMedicine()->getSku(),
                        'Dosis' => $requisition->getQuantity(),
                        'Suma de Piezas' => $piezas,
                        'Ciclo' => $requisition->getRequisition()->getCycle()->getName(),
                    );
                }
            }
        }


        $response = new JsonResponse($total);
        return $response;
//        return new JsonResponse([ $total], Response::HTTP_NOT_FOUND);
    }


    /**
     * @Route("/doctors/data", name="api_doctor_data")
     */
    public function data(RequisitionRepository $requisitionRepository, DoctorRepository $doctorRepository)
    {
        $keys = [1 => 'reconoce_cardiometabolico', 2 => 'reconoce_snc', 3 => 'experiencia_365'];
        $doctors = $doctorRepository->findBy(['isActive' => true]);

        foreach ($doctors as $doctor) {
            $params = ['id' => $doctor->getId(), 'idWS'=> $doctor->getIdWs(), 'name' => sprintf('%s %s %s', $doctor->getName(), $doctor->getLastName(), $doctor->getMotherName()), $keys[1] => false, $keys[2] => false, $keys[3] => false];
            $data[$doctor->getId()] = $params;
        }
        $dataDoctor = $requisitionRepository->getDataDoctor();
        foreach ($dataDoctor as $doctor) {
            $arr = &$data[$doctor['doctor_id']];
            $arr[$keys[$doctor['program_id']]] = $doctor['totales'] > 0 ? true : false;
        }
        //$data = ['totales' => 0, 'pages' => 10, 'current_page' => 1, 'next_page' => 2, 'prev_page' => null,'data' => []];


        return new JsonResponse(array_values($data));
    }

}
