<?php

namespace App\Controller;

use App\Entity\Requisition;
use App\Form\DataFilterType;
use App\Form\ManagerFilterType;
use App\Services\CycleManager;
use App\Services\DataHelper;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ManagerController extends AbstractController
{

    private $user;

    /**
     * @var DataHelper
     */
    private $dataHelper;

    public function __construct(Security $security, DataHelper $dataHelper)
    {
        $this->user = $security->getUser();
        $this->dataHelper = $dataHelper;
    }

    /**
     * @Route("/gerente", name="manager")
     */
    public function index(Request $request, DataHelper $dataHelper, CycleManager $cycleManager): Response
    {
        {
            $dataHelper->setManagerFilterParamers();
            $view = $request->isXmlHttpRequest() ? 'manager/_content_list.html.twig' : 'manager/index.html.twig';
            return $this->render($view, [
                'form' => $this->renderForm($request, ManagerFilterType::class),
                'users' => $this->dataHelper->getManagerRequisitions(),
                'shortcutsData' => $cycleManager->getCurrentCycle(),
            ]);
        }
    }

    public function renderForm(Request $request, $type = DataFilterType::class)
    {
        $predata = $this->dataHelper->getParameters();
        $form = $this->createForm($type, $predata);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $parameters = $form->getData();
            $this->dataHelper->setParameters($parameters);
        }

        return $form->createView();
    }

    /**
     * @Route("/export-f", name="export-f")
     */
    public function export(Request $request, DataHelper $dataHelper): Response
    {
        {
            $dataHelper->setManagerFilterParamers();
            $representantes = $this->dataHelper->getManagerRequisitions();
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setTitle("My First Worksheet");
            $writer = new Xlsx($spreadsheet);
            $headers = array('Iniciales Paciente', 'Doctor', 'Programa', 'Medicamento', 'Fecha', 'Ciclo', 'Status');
            $cols = array();
            $letter = 'A';

            while ($letter !== 'H') {
                $cols[] = $letter++;
            }
            foreach ($headers as $i => $header) {
                $sheet->setCellValue($cols[$i] . '1', $headers[$i]);
            }
            $sheet->setCellValue('H' . '1', 'Representante');

            $index = 2;
            foreach ($representantes as $representante) {
                $data = null;
//                dd($representante);
                if ($representante) {

                    $sheet->setCellValue('H' . $index, $representante->getName());

                    $index++;

                    foreach ($representante->getRequisitions() as $requisition) {
                        foreach ($requisition->getItems() as $item) {
                            $itemDesc = $item->getMedicine()->getName();
                            $data = array(
                                $requisition->getBeneficiary(),
                                $requisition->getDoctor(),
                                $requisition->getProgram(),
                                $itemDesc,
                                $requisition->getCreated(),
                                $requisition->getCycle(),
                                $this->getStatus($requisition)
                            );
                            foreach ($headers as $i => $header) {
                                if ($data && isset($data[$i])) {
                                    $sheet->setCellValue($cols[$i] . $index, $data[$i]);
                                }
                            }
                            if ($data) {
                                $index++;
                            }
                        }
                    }
                }


//                if ($data) {
//                    $index++;
//                }
            }


//            $data = array(
//                        $requisition->getRequisition()->getProgram()->getName(),
//                        $requisition->getRequisition()->getBeneficiary(),
//                        $paciente,
//                        $requisition->getRequisition()->getUser()->getRuta(),
//                        $requisition->getRequisition()->getUser()->getLinea(),
//                        $requisition->getRequisition()->getUser()->getDistrito(),
//                        $requisition->getRequisition()->getUser()->getName(),
//                        $requisition->getRequisition()->getUser()->getClientNumber(),
//                        $requisition->getRequisition()->getDoctor()->getName() . " " . $requisition->getRequisition()->getDoctor()->getLastName() . " " . $requisition->getRequisition()->getDoctor()->getMotherName(),
//                        $requisition->getRequisition()->getClave(),
//                        $statusRequisition[$requisition->getRequisition()->getStatus()],
//                        $requisition->getRequisition()->getDoctor()->getCodCrm(),
//                        $requisition->getRequisition()->getDoctor()->getSpeciality(),
//                        $requisition->getRequisition()->getCreated(),
//                        $requisition->getRequisition()->getUpdated(),
//                        $requisition->getRequisition()->getExpirationDate(),
//                        $requisition->getMedicine()->getName(),
//                        $requisition->getMedicine()->getSku(),
//                        $requisition->getQuantity(),
//                        $piezas,
//                        $requisition->getRequisition()->getCycle()->getName(),
////                        $entities && isset($entities[0]['total']) ? $entities[0]['total'] : 0,
//                    );


            $fileName = sprintf('reporte_%d.xlsx', time());

            $temp_file = tempnam(sys_get_temp_dir(), $fileName);
            $writer->save($temp_file);
            return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
            dd($requisitions);

            return $this->render($view, [
                'form' => $this->renderForm($request, ManagerFilterType::class),
                'users' => $this->dataHelper->getManagerRequisitions()
            ]);
        }
    }

    protected function getStatus(Requisition $requisition)
    {
        $options = ['renewed' => 'Renovada', 'canceled' => 'Cancelada', 'active' => 'Nueva','to_renovate' => 'Por renovar'];
        return @$options[$requisition->getStatus()];
    }

}
