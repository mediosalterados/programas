<?php


namespace App\Command;

use App\Entity\Doctor;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use JsonMachine\JsonDecoder\ErrorWrappingDecoder;
use JsonMachine\JsonDecoder\ExtJsonDecoder;
use JsonMachine\JsonMachine;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ImportDoctors extends Command
{
    protected static $defaultName = 'app:import-doctors';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    private $doctors;
    private $doctorsArray;
    private $persistentDoctors;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(string $name = null, EntityManagerInterface $entityManager)
    {
        ini_set('memory_limit',-1);
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        //ESTE CRON DEBERIA DE ESTAR CORRIENDO CADA 6 HORAS, QUE CORRA CON UNA HORA DE DIFERENCIA DE 1 HORA CON REPRESENTANTES
        $this
            ->setDescription('Importación de doctores desde el CMS')
            ->setHelp('Importación de doctores');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $data = JsonMachine::fromFile("https://smart-scale.net/ws_silanes_mx/medicos.php","",new ErrorWrappingDecoder(new ExtJsonDecoder()));
        $this->initializeDoctors();
        $this->persistentDoctors = [];

        $output->writeln(sprintf('Empiezo a leer WS'));
        foreach ($data as $key => $doctor) {
            if ($key instanceof DecodingError || $key instanceof DecodingError) {
                continue;
            }
            if(isset($doctor->Id_Unico)) {
                $existingDoctor = $this->filterDoctor($doctor->Cod_Med_CRM);
                $doctorToUpdate = $existingDoctor ? $existingDoctor : new Doctor();
                $this->setDoctor($doctorToUpdate, $doctor);
            }else{
                $output->writeln('No tengo datos correctos');
            }
        }
        $output->writeln(sprintf('Termino de leer WS'));


        $this->entityManager->flush();
        $this->removeDoctors($output);

        return Command::SUCCESS;
    }

    protected function removeDoctors(OutputInterface $output)
    {
        $output->writeln(sprintf('Elimino datos que no existen'));

        $toRemove = $this->doctors->filter(function (Doctor $doctor) {
            return !in_array($doctor->getId(), $this->persistentDoctors);
        });

        foreach ($toRemove as $doctor) {
            $doctor->setIsActive(false);
            $this->entityManager->persist($doctor);
        }

        $output->writeln(sprintf('Se mantienen %d registros', count($this->persistentDoctors)));
        $output->writeln(sprintf('Eliminamos %d registros', count($toRemove)));

        $this->entityManager->flush();

    }

    protected function initializeDoctors(){

        $this->doctors = new ArrayCollection($this->entityManager->getRepository(Doctor::class)->findAll());
        $doctors = [];
        foreach ($this->doctors as $entity){
            $doctors[$entity->getCodCrm()] = $entity;
        }
        $this->doctorsArray = $doctors;
    }

    protected function setDoctor(Doctor $doctor, $doctorInfo)
    {
        $doctor->setName($doctorInfo->Nombre);
        $doctor->setLastName($doctorInfo->A_Paterno);
        $doctor->setMotherName($doctorInfo->A_Materno);
        $doctor->setIdWs($doctorInfo->Id_Unico);
        $doctor->setSex($doctorInfo->Sexo);
        $doctor->setCatAudit($doctorInfo->Cat_Audit);
        $doctor->setSpeciality($doctorInfo->Especialidad_Primaria);
        $doctor->setAge($doctorInfo->Edad);
        $doctor->setEmail($doctorInfo->Email);
        $doctor->setCodCrm($doctorInfo->Cod_Med_CRM);
        $doctor->setCity($doctorInfo->{"Ciudad(Mun_Dele)"});
        $doctor->setRuta($doctorInfo->Ruta);
        $doctor->setRepresentantive($doctorInfo->Representante);
        $doctor->setIdentification($doctorInfo->Cedula);
        $doctor->setIsActive(true);
        $this->entityManager->persist($doctor);
        $this->persistentDoctors[] = $doctor->getId();
    }

    protected function filterDoctor($reference)
    {
        return isset($this->doctorsArray[$reference]) ? $this->doctorsArray[$reference]:null;
    }
}