<?php


namespace App\Command;


use App\Repository\RequisitionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Services\DataHelper;

class StatusRequisition extends Command
{
    private $dataHelper;
    protected static $defaultName = 'app:status-requisition';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var RequisitionRepository
     */
    private $requisitionRepository;

    public function __construct(string $name = null, EntityManagerInterface $entityManager, RequisitionRepository $requisitionRepository, DataHelper $dataHelper)
    {
        ini_set('memory_limit',-1);
        $this->entityManager = $entityManager;
        $this->dataHelper = $dataHelper;
        $this->requisitionRepository = $requisitionRepository;
        parent::__construct($name);
    }

    protected function configure()
    {
        //ESTE CRON DEBERIA DE ESTAR CORRIENDO CADA 24 HORAS, QUE CORRA EN LA MADRUGADA
        $this
            ->setDescription('Actualización de status para las requisiciones')
            ->setHelp('Actualización de status');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->transitionToRenovate($output);
        $this->transitionToCanceled($output, $this->dataHelper);
        $this->entityManager->flush();
        return Command::SUCCESS;
    }

    protected function transitionToRenovate(OutputInterface $output)
    {
        $data = $this->requisitionRepository->findToExpire();
        $output->writeln(sprintf('Van a expirar %d', count($data)));

        foreach ($data as $requisition) {
            $requisition->setStatus('to_renovate');
            $this->entityManager->persist($requisition);
        }
    }

    protected function transitionToCanceled(OutputInterface $output )
    {
        
        $cycleCurrent = $this->dataHelper->processCycleData();
        $actula = $cycleCurrent['cycle'];
        $data = $this->requisitionRepository->findToCanceled($cycleCurrent['cycle']);
        $output->writeln(sprintf('Se cancelan %d', count($data)));

        foreach ($data as $requisition) {
            $requisition->setStatus('canceled');
            $this->entityManager->persist($requisition);
        }
    }

}