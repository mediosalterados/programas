<?php


namespace App\Command;


use App\Entity\Cycle;
use App\Entity\Doctor;
use App\Entity\Medicine;
use App\Entity\Program;
use App\Entity\Requisition;
use App\Entity\RequisitionItem;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserData extends Command
{
    protected static $defaultName = 'app:generate-data';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(string $name = null, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $faker = Factory::create('es_MX');
        $this->clearDB();

        $user = $this->entityManager->getRepository(User::class)->find(4);
        $doctores = $this->entityManager->getRepository(Doctor::class)->findBy(array('ruta' => $user->getRuta()));
        $programas = $this->entityManager->getRepository(Program::class)->findAll();
        $medicine = $this->entityManager->getRepository(Medicine::class)->findAll();
        $cycle = new ArrayCollection($this->entityManager->getRepository(Cycle::class)->findAll());

        $randomDoctors = $faker->randomElements($doctores,rand(1,count($doctores)));
        $output->writeln(sprintf('Agrego %d doctores',count($randomDoctors)));

        $expiration = new \DateTime();
        $expiration->modify('+10 days');

        foreach($randomDoctors as $doctor){
            foreach ($programas as $programa){
                $max = rand(5,20);
                $output->writeln(sprintf('Agrego %d requisitions',$max));
                for($i=0;$i<$max;$i++){
                    $requisition = new Requisition();
                    $requisition->setUser($user);
                    $requisition->setProgram($programa);
                    $requisition->setBeneficiary(sprintf('%s%s%s',$faker->randomLetter,$faker->randomLetter,$faker->randomLetter));
                    $requisition->setdoctor($doctor);
                    $requisition->setCycle($cycle->last());
                    $requisition->setStatus('1');
                    $requisition->setExpirationDate($expiration);
                    $medicines = $faker->randomElements($medicine,rand(1,count($medicine)));
                    $output->writeln(sprintf('Agrego %d medicamentos',count($medicines)));
                    foreach ($medicines as $me){
                        $itemRequisition = new RequisitionItem();
                        $itemRequisition->setQuantity(rand(1,4));
                        $itemRequisition->setMedicine($me);
                        $requisition->addItem($itemRequisition);
                    }

                    $this->entityManager->persist($itemRequisition);
                    $this->entityManager->persist($requisition);
                }
            }
        }

        $this->entityManager->flush();
        return Command::SUCCESS;
    }

    public function clearDB(){
        $data = $this->entityManager->getRepository(Requisition::class)->findAll();
        foreach ($data as $entity){
            $this->entityManager->remove($entity);
        }
        $this->entityManager->flush();
    }

}