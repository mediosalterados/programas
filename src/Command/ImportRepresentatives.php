<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use JsonMachine\JsonMachine;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ImportRepresentatives extends Command
{
    protected static $defaultName = 'app:import-representatives';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    private $representatives;
    private $persistentRepresentatives;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(string $name = null, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        ini_set('memory_limit', -1);
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function configure()
    {
        //ESTE CRON DEBERIA DE ESTAR CORRIENDO CADA 6 HORAS, QUE CORRA CON UNA HORA DE DIFERENCIA DE 1 HORA CON DOCTORES
        $this
            ->setDescription('Importación de representantes desde el CMS')
            ->setHelp('Importación de representantes');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $data = JsonMachine::fromFile("https://smart-scale.net/ws_silanes_mx/representantes.php");
        $this->representatives = new ArrayCollection($this->entityManager->getRepository(User::class)->findAll());
        $this->persistentRepresentatives = [];
        foreach ($data as $representative) {
            $existingUser = $this->filterUser($representative)->last();
            $user = $existingUser ? $existingUser : new User();
            $this->setUser($user, $representative);
        }

        $this->entityManager->flush();
        $this->removeUsers($output);

        return Command::SUCCESS;
    }

    protected function removeUsers(OutputInterface $output)
    {

        $toRemove = $this->representatives->filter(function (User $user) {
            return !in_array($user->getId(), $this->persistentRepresentatives) && !in_array("ROLE_ADMIN", $user->getRoles());
        });

        foreach ($toRemove as $user) {
            $user->setIsActive(false);
            $this->entityManager->persist($user);
        }

        $output->writeln(sprintf('Se mantienen %d registros', count($this->persistentRepresentatives)));
        $output->writeln(sprintf('Eliminamos %d registros', count($toRemove)));

        $this->entityManager->flush();

    }

    protected function setUser(User $user, $representative)
    {
        $user->setName($representative['Representante']);
        $user->setRuta($representative['Ruta']);
        $user->setEmail($representative['E_mail']);
        $user->setLinea($representative['Linea']);
        $user->setDistrito($representative['Distrito']);
        $user->setClientNumber($representative['Num_Empleado']);
        $user->getIsActive(true);

        if (!$user->getPassword()) {
            $user->setPassword("INACTIVO");
        }

        $this->entityManager->persist($user);
        $this->persistentRepresentatives[] = $user->getId();
    }

    protected function filterUser($representative)
    {
        return $this->representatives->filter(function (User $user) use ($representative) {
            return $user->getEmail() == $representative['E_mail'];
        });
    }
}