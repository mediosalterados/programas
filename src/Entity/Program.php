<?php

namespace App\Entity;

use App\Repository\ProgramRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProgramRepository::class)
 */
class Program
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Requisition::class, mappedBy="program", orphanRemoval=true)
     */
    private $requisitions;

    /**
     * @ORM\Column(type="integer")
     */
    private $monthlyPeriodicity;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $lineas = [];

    public function __construct()
    {
        $this->requisitions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Requisition[]
     */
    public function getRequisitions(): Collection
    {
        return $this->requisitions;
    }

    public function addRequisition(Requisition $requisition): self
    {
        if (!$this->requisitions->contains($requisition)) {
            $this->requisitions[] = $requisition;
            $requisition->setProgram($this);
        }

        return $this;
    }

    public function removeRequisition(Requisition $requisition): self
    {
        if ($this->requisitions->removeElement($requisition)) {
            // set the owning side to null (unless already changed)
            if ($requisition->getProgram() === $this) {
                $requisition->setProgram(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getMonthlyPeriodicity(): ?int
    {
        return $this->monthlyPeriodicity;
    }

    public function setMonthlyPeriodicity(int $monthlyPeriodicity): self
    {
        $this->monthlyPeriodicity = $monthlyPeriodicity;

        return $this;
    }

    public function getLineas(): ?array
    {
        return $this->lineas;
    }

    public function setLineas(?array $lineas): self
    {
        $this->lineas = $lineas;

        return $this;
    }
}
