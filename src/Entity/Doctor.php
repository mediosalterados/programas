<?php

namespace App\Entity;

use App\Repository\DoctorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DoctorRepository::class)
 */
class Doctor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $motherName;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $idWs;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $sex;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $catAudit;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $speciality;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codCrm;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $ruta;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $representantive;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $identification;

    /**
     * @ORM\OneToMany(targetEntity=Requisition::class, mappedBy="doctor", orphanRemoval=true)
     */
    private $requisitions;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    public function __construct()
    {
        $this->requisitions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getMotherName(): ?string
    {
        return $this->motherName;
    }

    public function setMotherName(string $motherName): self
    {
        $this->motherName = $motherName;

        return $this;
    }

    public function getIdWs(): ?string
    {
        return $this->idWs;
    }

    public function setIdWs(string $idWs): self
    {
        $this->idWs = $idWs;

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getCatAudit(): ?string
    {
        return $this->catAudit;
    }

    public function setCatAudit(?string $catAudit): self
    {
        $this->catAudit = $catAudit;

        return $this;
    }

    public function getSpeciality(): ?string
    {
        return $this->speciality;
    }

    public function setSpeciality(string $speciality): self
    {
        $this->speciality = $speciality;

        return $this;
    }

    public function getAge(): ?string
    {
        return $this->age;
    }

    public function setAge(string $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCodCrm(): ?string
    {
        return $this->codCrm;
    }

    public function setCodCrm(string $codCrm): self
    {
        $this->codCrm = $codCrm;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getRuta(): ?string
    {
        return $this->ruta;
    }

    public function setRuta(string $ruta): self
    {
        $this->ruta = $ruta;

        return $this;
    }

    public function getRepresentantive(): ?string
    {
        return $this->representantive;
    }

    public function setRepresentantive(string $representantive): self
    {
        $this->representantive = $representantive;

        return $this;
    }

    public function getIdentification(): ?string
    {
        return $this->identification;
    }

    public function setIdentification(string $identification): self
    {
        $this->identification = $identification;

        return $this;
    }

    /**
     * @return Collection|Requisition[]
     */
    public function getRequisitions(): Collection
    {
        return $this->requisitions;
    }

    public function addRequisition(Requisition $requisition): self
    {
        if (!$this->requisitions->contains($requisition)) {
            $this->requisitions[] = $requisition;
            $requisition->setdoctor($this);
        }

        return $this;
    }

    public function removeRequisition(Requisition $requisition): self
    {
        if ($this->requisitions->removeElement($requisition)) {
            // set the owning side to null (unless already changed)
            if ($requisition->getdoctor() === $this) {
                $requisition->setdoctor(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return sprintf('%s %s %s',$this->getName(),$this->getLastName(),$this->getMotherName());
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
