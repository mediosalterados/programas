<?php

namespace App\Entity;

use App\Behavior\Timestampable;
use App\Repository\RequisitionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass=RequisitionRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Requisition
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $beneficiary;

    /**
     * @ORM\ManyToOne(targetEntity=Doctor::class, inversedBy="requisitions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $doctor;

    /**
     * @ORM\ManyToOne(targetEntity=Program::class, inversedBy="requisitions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $program;

    /**
     * @ORM\OneToMany(targetEntity=RequisitionItem::class, mappedBy="requisition", orphanRemoval=true, cascade={"persist"})
     */
    private $items;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="requisitions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $expirationDate;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=Cycle::class, inversedBy="requisitions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cycle;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $clave;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isCoupon = false;

    use Timestampable;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBeneficiary(): ?string
    {
        return $this->beneficiary;
    }

    public function setBeneficiary(string $beneficiary): self
    {
        $this->beneficiary = $beneficiary;

        return $this;
    }

    public function getdoctor(): ?Doctor
    {
        return $this->doctor;
    }

    public function setdoctor(?Doctor $doctor): self
    {
        $this->doctor = $doctor;

        return $this;
    }

    public function getProgram(): ?Program
    {
        return $this->program;
    }

    public function setProgram(?Program $program): self
    {
        $this->program = $program;

        return $this;
    }

    /**
     * @return Collection|RequisitionItem[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(RequisitionItem $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setRequisition($this);
        }

        return $this;
    }

    public function removeItem(RequisitionItem $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getRequisition() === $this) {
                $item->setRequisition(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getExpirationDate(): ?\DateTimeInterface
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(?\DateTimeInterface $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCycle(): ?Cycle
    {
        return $this->cycle;
    }

    public function setCycle(?Cycle $cycle): self
    {
        $this->cycle = $cycle;

        return $this;
    }

    public function getItemsDescription(){
        $prescription = [];
        foreach ($this->getItems() as $item){
            $prescription[] = $item->getMedicine();
        }
        return $prescription;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(?string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }
     /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $unique = explode('-',Uuid::uuid4()->toString());
        $this->clave  = strtoupper(sprintf($unique[0],$this->getId()));
    }

    public function getIsCoupon(): ?bool
    {
        return $this->isCoupon;
    }

    public function setIsCoupon(bool $isCoupon): self
    {
        $this->isCoupon = $isCoupon;

        return $this;
    }
}
