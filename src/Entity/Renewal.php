<?php

namespace App\Entity;

use App\Repository\RenewalRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RenewalRepository::class)
 */
class Renewal
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=requisition::class)
     */
    private $requisition;

    /**
     * @ORM\ManyToOne(targetEntity=cycle::class)
     */
    private $cycle;

    /**
     * @ORM\Column(type="date")
     */
    private $renovation_date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRequisition(): ?requisition
    {
        return $this->requisition;
    }

    public function setRequisition(?requisition $requisition): self
    {
        $this->requisition = $requisition;

        return $this;
    }

    public function getCycle(): ?cycle
    {
        return $this->cycle;
    }

    public function setCycle(?cycle $cycle): self
    {
        $this->cycle = $cycle;

        return $this;
    }

    public function getRenovationDate(): ?\DateTimeInterface
    {
        return $this->renovation_date;
    }

    public function setRenovationDate(\DateTimeInterface $renovation_date): self
    {
        $this->renovation_date = $renovation_date;

        return $this;
    }
}
