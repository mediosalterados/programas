<?php

namespace App\Entity;

use App\Behavior\Timestampable;
use App\Repository\BrandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
/**
 * @ORM\Entity(repositoryClass=BrandRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class Brand
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    use Timestampable;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $frontImage;

    /**
     * @Vich\UploadableField(mapping="cupon_design", fileNameProperty="frontImage")
     * @var File
     */
    private $frontImageFile;

    public function setFrontImageFile(File $image = null)
    {
        $this->frontImageFile = $image;

        if ($image) {
            $this->updated = new \DateTime('now');
        }
    }

    public function getFrontImageFile()
    {
        return $this->frontImageFile;
    }


    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $backImage;

    /**
     * @Vich\UploadableField(mapping="cupon_design", fileNameProperty="backImage")
     * @var File
     */
    private $backImageFile;

    /**
     * @ORM\OneToMany(targetEntity=Campaign::class, mappedBy="brand", orphanRemoval=true)
     */
    private $campaigns;

    public function __construct()
    {
        $this->campaigns = new ArrayCollection();
    }

    public function setBackImageFile(File $image = null)
    {
        $this->backImageFile = $image;

        if ($image) {
            $this->updated = new \DateTime('now');
        }
    }

    public function getBackImageFile()
    {
        return $this->backImageFile;
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(?\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @return Collection|Campaign[]
     */
    public function getCampaigns(): Collection
    {
        return $this->campaigns;
    }

    public function addCampaign(Campaign $campaign): self
    {
        if (!$this->campaigns->contains($campaign)) {
            $this->campaigns[] = $campaign;
            $campaign->setBrand($this);
        }

        return $this;
    }

    public function removeCampaign(Campaign $campaign): self
    {
        if ($this->campaigns->contains($campaign)) {
            $this->campaigns->removeElement($campaign);
            // set the owning side to null (unless already changed)
            if ($campaign->getBrand() === $this) {
                $campaign->setBrand(null);
            }
        }

        return $this;
    }

    public function getFrontImage(): ?string
    {
        return $this->frontImage;
    }

    public function setFrontImage(?string $frontImage): self
    {
        $this->frontImage = $frontImage;

        return $this;
    }

    public function getBackImage(): ?string
    {
        return $this->backImage;
    }

    public function setBackImage(?string $backImage): self
    {
        $this->backImage = $backImage;

        return $this;
    }


    public function __toString()
    {
        return $this->getName();
    }
}
