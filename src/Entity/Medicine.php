<?php

namespace App\Entity;

use App\Repository\MedicineRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MedicineRepository::class)
 */
class Medicine
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=RequisitionItem::class, mappedBy="medicine", orphanRemoval=true)
     */
    private $requisitions;

    /**
     * @ORM\Column(type="integer")
     */
    private $maximumPerDose;

    /**
     * @ORM\OneToMany(targetEntity=MedicinePieces::class, mappedBy="medicine", orphanRemoval=true)
     */
    private $pieces;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $lineas = [];

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $sku;

    /**
     * @ORM\Column(type="integer",nullable=false)
     */
    private $maximumPerCoupon;



    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->requisitions = new ArrayCollection();
        $this->pieces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|RequisitionItem[]
     */
    public function getRequisitions(): Collection
    {
        return $this->requisitions;
    }

    public function addRequisition(RequisitionItem $requisition): self
    {
        if (!$this->requisitions->contains($requisition)) {
            $this->requisitions[] = $requisition;
            $requisition->setMedicine($this);
        }

        return $this;
    }

    public function removeRequisition(RequisitionItem $requisition): self
    {
        if ($this->requisitions->removeElement($requisition)) {
            // set the owning side to null (unless already changed)
            if ($requisition->getMedicine() === $this) {
                $requisition->setMedicine(null);
            }
        }

        return $this;
    }


    public function  __toString()
    {
        return $this->getName();
    }

    public function getMaximumPerDose(): ?int
    {
        return $this->maximumPerDose;
    }

    public function setMaximumPerDose(int $maximumPerDose): self
    {
        $this->maximumPerDose = $maximumPerDose;

        return $this;
    }

    /**
     * @return Collection|MedicinePieces[]
     */
    public function getPieces(): Collection
    {
        return $this->pieces;
    }

    public function addPiece(MedicinePieces $piece): self
    {
        if (!$this->pieces->contains($piece)) {
            $this->pieces[] = $piece;
            $piece->setMedicine($this);
        }

        return $this;
    }

    public function removePiece(MedicinePieces $piece): self
    {
        if ($this->pieces->removeElement($piece)) {
            // set the owning side to null (unless already changed)
            if ($piece->getMedicine() === $this) {
                $piece->setMedicine(null);
            }
        }

        return $this;
    }

    public function getLineas(): ?array
    {
        return $this->lineas;
    }

    public function setLineas(?array $lineas): self
    {
        $this->lineas = $lineas;

        return $this;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(?string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getMaximumPerCoupon(): ?int
    {
        return $this->maximumPerCoupon;
    }

    public function setMaximumPerCoupon(int $maximumPerCoupon): self
    {
        $this->maximumPerCoupon = $maximumPerCoupon;

        return $this;
    }
}
