<?php

namespace App\Entity;

use App\Behavior\Timestampable;
use App\Repository\RequestItemRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RequestItemRepository::class)
 */
class RequisitionItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Requisition::class, inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
    private $requisition;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity=Medicine::class, inversedBy="requisitions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $medicine;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pieces;

    use Timestampable;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRequisition(): ?Requisition
    {
        return $this->requisition;
    }

    public function setRequisition(?Requisition $requisition): self
    {
        $this->requisition = $requisition;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getMedicine(): ?Medicine
    {
        return $this->medicine;
    }

    public function setMedicine(?Medicine $medicine): self
    {
        $this->medicine = $medicine;

        return $this;
    }

    public function getPieces(): ?int
    {
        return $this->pieces;
    }

    public function setPieces(?int $pieces): self
    {
        $this->pieces = $pieces;

        return $this;
    }


}
