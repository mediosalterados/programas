<?php

namespace App\Entity;

use App\Behavior\Timestampable;
use App\Repository\CampaignRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=CampaignRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class Campaign
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $file;

    /**
     * @Vich\UploadableField(mapping="xls_file", fileNameProperty="file")
     * @var File
     */
    private $xlsFile;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)
     */
    private $expiration;

    /**
     * @ORM\ManyToOne(targetEntity=Brand::class, inversedBy="campaigns")
     * @ORM\JoinColumn(nullable=false)
     */
    private $brand;

    /**
     * @ORM\OneToMany(targetEntity=CampaignCode::class, mappedBy="campaign", orphanRemoval=true)
     */
    private $codes;

    public function __construct()
    {
        $this->codes = new ArrayCollection();
    }

    use Timestampable;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function setXlsFile(File $image = null)
    {
        $this->xlsFile = $image;

        if ($image) {
            $this->updated = new \DateTime('now');
        }
    }

    public function getXlsFile()
    {
        return $this->xlsFile;
    }

    public function getExpiration(): ?string
    {
        return $this->expiration;
    }

    public function setExpiration(string $expiration): self
    {
        $this->expiration = $expiration;

        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return Collection|CampaignCode[]
     */
    public function getCodes(): Collection
    {
        return $this->codes;
    }

    public function addCode(CampaignCode $code): self
    {
        if (!$this->codes->contains($code)) {
            $this->codes[] = $code;
            $code->setCampaign($this);
        }

        return $this;
    }

    public function removeCode(CampaignCode $code): self
    {
        if ($this->codes->contains($code)) {
            $this->codes->removeElement($code);
            // set the owning side to null (unless already changed)
            if ($code->getCampaign() === $this) {
                $code->setCampaign(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return 'Campaña ' . (string)$this->getId();
    }
}
