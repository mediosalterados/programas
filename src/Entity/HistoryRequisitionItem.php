<?php

namespace App\Entity;

use App\Repository\HistoryRequisitionItemRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HistoryRequisitionItemRepository::class)
 */
class HistoryRequisitionItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Requisition::class)
     */
    private $requisition;

    /**
     * @ORM\ManyToOne(targetEntity=Medicine::class)
     */
    private $medicine;

    /**
     * @ORM\ManyToOne(targetEntity=Cycle::class)
     */
    private $current_cycle;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRequisition(): ?Requisition
    {
        return $this->requisition;
    }

    public function setRequisition(?Requisition $requisition): self
    {
        $this->requisition = $requisition;

        return $this;
    }

    public function getMedicine(): ?Medicine
    {
        return $this->medicine;
    }

    public function setMedicine(?Medicine $medicine): self
    {
        $this->medicine = $medicine;

        return $this;
    }

    public function getCurrentCycle(): ?Cycle
    {
        return $this->current_cycle;
    }

    public function setCurrentCycle(?Cycle $current_cycle): self
    {
        $this->current_cycle = $current_cycle;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
