<?php


namespace App\Message;


class GeneratePdf
{

    /**
     */
    private $code;

    public function __construct($code)
    {
        $this->code = $code;
    }

    public function getCode()
    {
        return $this->code;
    }
}