<?php


namespace App\Message;


use App\Entity\Campaign;

class ImportCode
{

    /**
     * @var Campaign
     */
    private $campaign;

    public function __construct($campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * @return Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }
}