<?php

namespace App\DataFixtures;

use App\Entity\Doctor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class DoctorFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('es_MX');

        for ($i = 0; $i < rand(50, 200); $i++) {
            $doctor = new Doctor();
            $doctor->setName($faker->name);
            $doctor->setEmail($faker->email);
            $doctor->setAge($faker->randomDigit);
            $doctor->setCatAudit($faker->randomLetter);
            $doctor->setCity($faker->city);
            $doctor->setCodCrm($faker->postcode);
            $doctor->setSpeciality($faker->jobTitle);
            $doctor->setIdentification($faker->uuid);
            $doctor->setIdWs($faker->numerify());
            $doctor->setRepresentantive($faker->title());
            $doctor->setLastName($faker->lastName);
            $doctor->setMotherName($faker->lastName);
            $doctor->setRuta($faker->city);
            $doctor->setSex($faker->randomElement(array('male','female')));
            $manager->persist($doctor);
        }

        $manager->flush();
    }
}
