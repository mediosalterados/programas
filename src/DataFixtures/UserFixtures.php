<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Utils\ImagesList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    protected $encoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->encoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setName('Guillermo');
        $user->setEmail('guillermo@spraystudio.com.mx');
        $password = $this->encoder->encodePassword($user, '56fubu124817');
        $user->setPassword($password);
        $user->setRoles(['ROLE_ADMIN']);
        $user->setIsVerified(true);
        $user->setRuta('0001');
        $user->setDistrito('0111');
        $user->setLinea('01232');
        $manager->persist($user);

        $manager->flush();
    }
}
