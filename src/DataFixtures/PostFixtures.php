<?php

namespace App\DataFixtures;

use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class PostFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $images = [
            "alyssum-mormino-Hlv9LUPF83A-unsplash.jpg",
            "artur-tumasjan-qLzWvcQq-V8-unsplash.jpg",
            "averie-woodard-4nulm-JUYFo-unsplash.jpg",
            "bruce-mars-Q_iJuyRdy4c-unsplash.jpg",
            "cdc--V-nbUmDfzM-unsplash.jpg",
            "cdc-_N7I1JyPYJw-unsplash.jpg",
            "christina-victoria-craft-WHSnkIwWpec-unsplash.jpg",
            "james-yarema-5tyMgag0wRo-unsplash.jpg",
            "jasmin-chew-tEx-meWmEMc-unsplash.jpg",
            "johen-redman-ohTNGtRMSBo-unsplash.jpg",
            "kelly-sikkema-r2hTBxEkgWQ-unsplash.jpg",
            "luke-mckeown-JmtLeUYaBEU-unsplash.jpg",
            "nong-vang-0b4QY1ZkdYg-unsplash.jpg",
            "owen-beard-DK8jXx1B-1c-unsplash.jpg"
        ];

        $faker = Factory::create();
        for ($i = 0; $i < rand(50, 100); $i++) {
            $post = new Post();
            $post->setTitle($faker->words(3, true));
            $post->setContent($faker->paragraphs(4, true));
            $post->setResume($faker->paragraph);
            $post->setImage($faker->randomElement($images));
            $manager->persist($post);
        }

        $manager->flush();
    }
}
