<?php

namespace App\DataFixtures;

use App\Entity\Doctor;
use App\Entity\Medicine;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class MedicineFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('es_MX');

        for ($i = 0; $i < 2; $i++) {
            $medicine = new Medicine();
            $medicine->setName($faker->sentence);
            $manager->persist($medicine);
        }

        $manager->flush();
    }
}
