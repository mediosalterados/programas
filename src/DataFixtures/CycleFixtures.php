<?php

namespace App\DataFixtures;

use App\Entity\Cycle;
use App\Entity\User;
use App\Utils\ImagesList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CycleFixtures extends Fixture
{



    public function load(ObjectManager $manager)
    {

        $startDate = new \DateTime();
        $endDate = new \DateTime();
        $endDate->modify('+45 days');
        $cycle = new Cycle();
        $cycle->setName('Ciclo unico');
        $cycle->setStartDate($startDate);
        $cycle->setEndDate($endDate);
        $manager->persist($cycle);
        $manager->flush();
    }
}
