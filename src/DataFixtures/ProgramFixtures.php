<?php

namespace App\DataFixtures;

use App\Entity\Doctor;
use App\Entity\Medicine;
use App\Entity\Program;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ProgramFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('es_MX');

        for ($i = 0; $i < 2; $i++) {
            $program = new Program();
            $program->setName($faker->sentence);
            $manager->persist($program);
        }

        $manager->flush();
    }
}
