<?php


namespace App\Services;


use App\Entity\Campaign;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Yectep\PhpSpreadsheetBundle\Factory;

class XlsImporter
{

    private $factory;
    private $uploaderHelper;

    public function __construct(Factory $factory,UploaderHelper $uploaderHelper)
    {
        $this->factory = $factory;
        $this->uploaderHelper = $uploaderHelper;
    }

    public function fetchData(Campaign $campaign)
    {
        $path = $this->uploaderHelper->asset($campaign, 'xlsFile');
        $readerXlsx = $this->factory->createReader('Xlsx');
        $filePath = __DIR__.'/../../public'.$path;
        $spreadsheet = $readerXlsx->load($filePath);
        $worksheet = $spreadsheet->getActiveSheet()->toArray();
        return $worksheet;
    }
}