<?php

namespace App\Services;

use App\Entity\Program;
use Symfony\Component\Security\Core\Security;

class RequisitionHelper {

    /**
     * @var Security
     */
    private $security;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(Security $security) {
        $this->security = $security;
    }

    public function calculateExpirationDate(Program $program, $date = null) {
        $endDate = $date ? clone $date : new \DateTime();

        if ($program->getMonthlyPeriodicity() <= 1) {
            $endDate->modify(sprintf('+%d days', '45'));
        } else {
            $endDate->modify(sprintf('+%d months', $program->getMonthlyPeriodicity()));
        }
//        dd($endDate);
        return $endDate;
    }

}