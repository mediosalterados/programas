<?php


namespace App\Services;


use App\Entity\CampaignCode;
use Knp\Snappy\Pdf;
use Twig\Environment;

class PdfGenerator
{

    private $knpSnappyPdf;
    private $templating;
    private $pdfPath;

    public function __construct(Pdf $knpSnappyPdf, Environment $templating,$pdfPath)
    {
        $this->knpSnappyPdf = $knpSnappyPdf;
        $this->templating = $templating;
        $this->pdfPath = $pdfPath;
    }

    public function generatePdf(CampaignCode $code)
    {
        $html = $this->templating->render('pdf/coupon_base.html.twig', ['code' => $code]);
        $this->knpSnappyPdf->generateFromHtml($html,sprintf('%sc%s/%s.pdf',$this->pdfPath,$code->getCampaign()->getId(),$code->getId()));
    }
}