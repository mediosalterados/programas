<?php

namespace App\Services;

use App\Entity\Cycle;
use App\Repository\DoctorRepository;
use App\Repository\MedicineRepository;
use App\Repository\RequisitionRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class DataHelper
{

    private $cycleManager;
    private $user;
    private $doctorRepository;
    private $medicineRepository;
    private $filterParameters = null;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var Security
     */
    private $security;

    public function __construct(CycleManager $cycleManager, UserRepository $userRepository, Security $security, DoctorRepository $doctorRepository, MedicineRepository $medicineRepository, RequisitionRepository $requisitionRepository, SessionInterface $session)
    {
        $this->cycleManager = $cycleManager;
        $this->user = $security->getUser();
        $this->doctorRepository = $doctorRepository;
        $this->medicineRepository = $medicineRepository;
        $this->requisitionRepository = $requisitionRepository;
        $this->userRepository = $userRepository;
        $this->session = $session;
        $this->security = $security;

        $sessionParameters = $this->session->get('manager_filters');
        if ($sessionParameters and isset($sessionParameters['ciclos']) ) {
            if ($sessionParameters['ciclos']) {
                $sessionParameters['ciclos'] = $cycleManager->getCycle($sessionParameters['ciclos']);
            }

            if ($sessionParameters['medico']) {
                $sessionParameters['medico'] = $doctorRepository->find($sessionParameters['medico']);
            }

            $this->filterParameters = $sessionParameters;

        }

    }

    public function setParameters($params)
    {
        $this->filterParameters = $params;
        $this->session->set('manager_filters', $params);
    }

    public function getParameters()
    {
        return $this->filterParameters;
    }

    public function getActiveRequisitions()
    {
        $this->filterParameters['isActive'] = true;
        return $this->doctorRepository->findByUser($this->user, $this->filterParameters, false);
    }

    public function getActiveTotalRequisitions()
    {
        $this->filterParameters['isActive'] = true;
        return $this->requisitionRepository->findByUser($this->user, $this->filterParameters);
    }

    public function getToExpireRequisitions()
    {
        return $this->doctorRepository->findByUser($this->user, $this->filterParameters, true);
    }

    public function getToExpireTotalRequisitions()
    {
        return $this->requisitionRepository->findByUserToExpireTotal($this->user, $this->filterParameters);
    }

    public function getAllRequisitions()
    {
        $this->filterParameters['isHistoric'] = true;
        return $this->doctorRepository->findByUser($this->user, $this->filterParameters);
    }

    public function getRequisitionByMedicineTotal()
    {
        $ciclo = $this->filterParameters && isset($this->filterParameters['ciclo']) ? $this->filterParameters['ciclo'] : $this->cycleManager->getCurrentCycle();
        $entities = $this->medicineRepository->findByUser($this->user, $ciclo, false);
        return $entities && isset($entities[0]['total']) ? $entities[0]['total'] : 0;
    }

    public function setManagerFilterParamers()
    {
        if (!$this->filterParameters) {
            $this->filterParameters = [];
        }
    }

    public function getManagerRequisitions()
    {
        if (!isset($this->filterParameters['ciclos']) || $this->filterParameters['ciclos'] == "") {
            $this->filterParameters['ciclos'] = $this->cycleManager->getCurrentCycle();
        }

        return $this->userRepository->findAllByDistrito($this->user, $this->filterParameters);
    }

    public function getActiveRequisitionsByMedicine()
    {
        $ciclo = $this->filterParameters && isset($this->filterParameters['ciclo']) ? $this->filterParameters['ciclo'] : $this->cycleManager->getCurrentCycle();
        $entities = $this->medicineRepository->findByUser($this->user, $ciclo, true);
        foreach ($entities as $key => $entity) {
            $entities[$key]['data'] = $this->doctorRepository->findByMedicine($entity['medicine_id'], $this->user, $ciclo);
        }
        return $entities;
    }

    public function processCycleData()
    {
        /* @var Cycle $cycle */
        $now = new \DateTime();
        $cycle = $this->cycleManager->getCurrentCycle();
        if ($cycle) {
            $interval = $cycle->getStartDate()->diff($cycle->getEndDate());
            $missing = $now->diff($cycle->getEndDate());
            $percent = 100 - ($missing->format('%a') / $interval->format('%a') * 100);
        } else {
            $percent = 0;
        }


        return ['percent' => $percent, 'cycle' => $cycle];
    }

    public function getShortCutsData()
    {
        return [
            'renew_total' => count($this->getToExpireTotalRequisitions()),
            'active_total' => count($this->getActiveTotalRequisitions()),
            'history_total' => count($this->getAllRequisitions()),
            'medicines_total' => $this->getRequisitionByMedicineTotal(),
            'cycle_data' => $this->processCycleData()
        ];
    }

}
