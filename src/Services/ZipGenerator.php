<?php


namespace App\Services;


use App\Entity\Campaign;
use PhpZip\Exception\ZipException;
use PhpZip\ZipFile;

class ZipGenerator
{
    public function zipCoupons(Campaign $campaign){
        $zipFile = new ZipFile();
        try{
            $zipFile->addDir(sprintf('coupons/c%s',$campaign->getId()))->outputAsAttachment(sprintf('coupons-c%s.zip',$campaign->getId()));
        }catch (ZipException $e){

        } finally {
            $zipFile->close();
        }
    }
}