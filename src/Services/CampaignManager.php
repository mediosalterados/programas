<?php


namespace App\Services;


use App\Entity\Campaign;
use App\Entity\CampaignCode;
use App\Message\GeneratePdf;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class CampaignManager
{

    private $xlsImporter;
    private $em;
    private $pdfGenerator;
    private $messageBus;
    public function __construct(XlsImporter $xlsImporter,EntityManagerInterface $em,MessageBusInterface $messageBus)
    {
        $this->xlsImporter = $xlsImporter;
        $this->em = $em;
        $this->messageBus = $messageBus;
    }

    public function addCodes(Campaign $campaign){
       $data = $this->xlsImporter->fetchData($campaign);
       foreach($data as $element){
           if(!empty($element[0]) && !empty($element[1])) {
               $code = new CampaignCode();
               $code->setCampaign($campaign);
               $code->setCode($element[1]);
               $code->setProduct($element[0]);
               $code->setBarcode($this->generateBarCode($code));
               $campaign->addCode($code);
               $this->em->persist($code);
           }
       }
        $this->em->flush();
       $this->exportCodes($campaign);
    }

    public function exportCodes(Campaign $campaign){
        foreach ($campaign->getCodes() as $code){
            $message = new GeneratePdf($code->getId());
            $this->messageBus->dispatch($message);
        }
    }

    protected function generateBarCode(CampaignCode $code){
        //https://github.com/codeitnowin/barcode-generator
        $barcode = new BarcodeGenerator();
        $barcode->setText($code->getCode());
        $barcode->setType(BarcodeGenerator::Code128);
        $barcode->setScale(5);
        $barcode->setThickness(20);
        $barcode->setFontSize(12);
        return $barcode->generate();
    }

}
