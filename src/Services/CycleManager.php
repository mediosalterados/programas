<?php


namespace App\Services;


use App\Entity\Cycle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class CycleManager
{


    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    private $useCurrentCycle = false;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }



    public function getCurrentCycle(){
        $date = new \DateTime();
        $entities = new ArrayCollection($this->entityManager->getRepository(Cycle::class)->findCurrentCycle($date));
        return $entities->count()>0 ? $entities->last():null;
    }

    /**
     * @return bool
     */
    public function isUseCurrentCycle(): bool
    {
        return $this->useCurrentCycle;
    }

    /**
     * @param bool $useCurrentCycle
     */
    public function setUseCurrentCycle(bool $useCurrentCycle): void
    {
        $this->useCurrentCycle = $useCurrentCycle;
    }

    public function getCycle($id){
       return $this->entityManager->getRepository(Cycle::class)->find($id);
    }
}